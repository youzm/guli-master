package com.carl.guli.model;

import java.io.Serializable;

public class FeedbackWithBLOBs extends Feedback implements Serializable {
    private String text;

    private String answer;

    private static final long serialVersionUID = 1L;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text == null ? null : text.trim();
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer == null ? null : answer.trim();
    }
}