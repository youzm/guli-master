package com.carl.guli.model;

import java.io.Serializable;

public class ScenicSpotWithBLOBs extends ScenicSpot implements Serializable {
    private String text;

    private String textPic;

    private static final long serialVersionUID = 1L;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text == null ? null : text.trim();
    }

    public String getTextPic() {
        return textPic;
    }

    public void setTextPic(String textPic) {
        this.textPic = textPic == null ? null : textPic.trim();
    }
}