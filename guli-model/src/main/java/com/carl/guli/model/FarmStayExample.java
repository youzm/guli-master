package com.carl.guli.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class FarmStayExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public FarmStayExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andFarmIdIsNull() {
            addCriterion("farm_id is null");
            return (Criteria) this;
        }

        public Criteria andFarmIdIsNotNull() {
            addCriterion("farm_id is not null");
            return (Criteria) this;
        }

        public Criteria andFarmIdEqualTo(Integer value) {
            addCriterion("farm_id =", value, "farmId");
            return (Criteria) this;
        }

        public Criteria andFarmIdNotEqualTo(Integer value) {
            addCriterion("farm_id <>", value, "farmId");
            return (Criteria) this;
        }

        public Criteria andFarmIdGreaterThan(Integer value) {
            addCriterion("farm_id >", value, "farmId");
            return (Criteria) this;
        }

        public Criteria andFarmIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("farm_id >=", value, "farmId");
            return (Criteria) this;
        }

        public Criteria andFarmIdLessThan(Integer value) {
            addCriterion("farm_id <", value, "farmId");
            return (Criteria) this;
        }

        public Criteria andFarmIdLessThanOrEqualTo(Integer value) {
            addCriterion("farm_id <=", value, "farmId");
            return (Criteria) this;
        }

        public Criteria andFarmIdIn(List<Integer> values) {
            addCriterion("farm_id in", values, "farmId");
            return (Criteria) this;
        }

        public Criteria andFarmIdNotIn(List<Integer> values) {
            addCriterion("farm_id not in", values, "farmId");
            return (Criteria) this;
        }

        public Criteria andFarmIdBetween(Integer value1, Integer value2) {
            addCriterion("farm_id between", value1, value2, "farmId");
            return (Criteria) this;
        }

        public Criteria andFarmIdNotBetween(Integer value1, Integer value2) {
            addCriterion("farm_id not between", value1, value2, "farmId");
            return (Criteria) this;
        }

        public Criteria andNameIsNull() {
            addCriterion("name is null");
            return (Criteria) this;
        }

        public Criteria andNameIsNotNull() {
            addCriterion("name is not null");
            return (Criteria) this;
        }

        public Criteria andNameEqualTo(String value) {
            addCriterion("name =", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotEqualTo(String value) {
            addCriterion("name <>", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameGreaterThan(String value) {
            addCriterion("name >", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameGreaterThanOrEqualTo(String value) {
            addCriterion("name >=", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLessThan(String value) {
            addCriterion("name <", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLessThanOrEqualTo(String value) {
            addCriterion("name <=", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLike(String value) {
            addCriterion("name like", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotLike(String value) {
            addCriterion("name not like", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameIn(List<String> values) {
            addCriterion("name in", values, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotIn(List<String> values) {
            addCriterion("name not in", values, "name");
            return (Criteria) this;
        }

        public Criteria andNameBetween(String value1, String value2) {
            addCriterion("name between", value1, value2, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotBetween(String value1, String value2) {
            addCriterion("name not between", value1, value2, "name");
            return (Criteria) this;
        }

        public Criteria andFarmPicIsNull() {
            addCriterion("farm_pic is null");
            return (Criteria) this;
        }

        public Criteria andFarmPicIsNotNull() {
            addCriterion("farm_pic is not null");
            return (Criteria) this;
        }

        public Criteria andFarmPicEqualTo(String value) {
            addCriterion("farm_pic =", value, "farmPic");
            return (Criteria) this;
        }

        public Criteria andFarmPicNotEqualTo(String value) {
            addCriterion("farm_pic <>", value, "farmPic");
            return (Criteria) this;
        }

        public Criteria andFarmPicGreaterThan(String value) {
            addCriterion("farm_pic >", value, "farmPic");
            return (Criteria) this;
        }

        public Criteria andFarmPicGreaterThanOrEqualTo(String value) {
            addCriterion("farm_pic >=", value, "farmPic");
            return (Criteria) this;
        }

        public Criteria andFarmPicLessThan(String value) {
            addCriterion("farm_pic <", value, "farmPic");
            return (Criteria) this;
        }

        public Criteria andFarmPicLessThanOrEqualTo(String value) {
            addCriterion("farm_pic <=", value, "farmPic");
            return (Criteria) this;
        }

        public Criteria andFarmPicLike(String value) {
            addCriterion("farm_pic like", value, "farmPic");
            return (Criteria) this;
        }

        public Criteria andFarmPicNotLike(String value) {
            addCriterion("farm_pic not like", value, "farmPic");
            return (Criteria) this;
        }

        public Criteria andFarmPicIn(List<String> values) {
            addCriterion("farm_pic in", values, "farmPic");
            return (Criteria) this;
        }

        public Criteria andFarmPicNotIn(List<String> values) {
            addCriterion("farm_pic not in", values, "farmPic");
            return (Criteria) this;
        }

        public Criteria andFarmPicBetween(String value1, String value2) {
            addCriterion("farm_pic between", value1, value2, "farmPic");
            return (Criteria) this;
        }

        public Criteria andFarmPicNotBetween(String value1, String value2) {
            addCriterion("farm_pic not between", value1, value2, "farmPic");
            return (Criteria) this;
        }

        public Criteria andConsumeIsNull() {
            addCriterion("consume is null");
            return (Criteria) this;
        }

        public Criteria andConsumeIsNotNull() {
            addCriterion("consume is not null");
            return (Criteria) this;
        }

        public Criteria andConsumeEqualTo(Float value) {
            addCriterion("consume =", value, "consume");
            return (Criteria) this;
        }

        public Criteria andConsumeNotEqualTo(Float value) {
            addCriterion("consume <>", value, "consume");
            return (Criteria) this;
        }

        public Criteria andConsumeGreaterThan(Float value) {
            addCriterion("consume >", value, "consume");
            return (Criteria) this;
        }

        public Criteria andConsumeGreaterThanOrEqualTo(Float value) {
            addCriterion("consume >=", value, "consume");
            return (Criteria) this;
        }

        public Criteria andConsumeLessThan(Float value) {
            addCriterion("consume <", value, "consume");
            return (Criteria) this;
        }

        public Criteria andConsumeLessThanOrEqualTo(Float value) {
            addCriterion("consume <=", value, "consume");
            return (Criteria) this;
        }

        public Criteria andConsumeIn(List<Float> values) {
            addCriterion("consume in", values, "consume");
            return (Criteria) this;
        }

        public Criteria andConsumeNotIn(List<Float> values) {
            addCriterion("consume not in", values, "consume");
            return (Criteria) this;
        }

        public Criteria andConsumeBetween(Float value1, Float value2) {
            addCriterion("consume between", value1, value2, "consume");
            return (Criteria) this;
        }

        public Criteria andConsumeNotBetween(Float value1, Float value2) {
            addCriterion("consume not between", value1, value2, "consume");
            return (Criteria) this;
        }

        public Criteria andLocationIsNull() {
            addCriterion("location is null");
            return (Criteria) this;
        }

        public Criteria andLocationIsNotNull() {
            addCriterion("location is not null");
            return (Criteria) this;
        }

        public Criteria andLocationEqualTo(String value) {
            addCriterion("location =", value, "location");
            return (Criteria) this;
        }

        public Criteria andLocationNotEqualTo(String value) {
            addCriterion("location <>", value, "location");
            return (Criteria) this;
        }

        public Criteria andLocationGreaterThan(String value) {
            addCriterion("location >", value, "location");
            return (Criteria) this;
        }

        public Criteria andLocationGreaterThanOrEqualTo(String value) {
            addCriterion("location >=", value, "location");
            return (Criteria) this;
        }

        public Criteria andLocationLessThan(String value) {
            addCriterion("location <", value, "location");
            return (Criteria) this;
        }

        public Criteria andLocationLessThanOrEqualTo(String value) {
            addCriterion("location <=", value, "location");
            return (Criteria) this;
        }

        public Criteria andLocationLike(String value) {
            addCriterion("location like", value, "location");
            return (Criteria) this;
        }

        public Criteria andLocationNotLike(String value) {
            addCriterion("location not like", value, "location");
            return (Criteria) this;
        }

        public Criteria andLocationIn(List<String> values) {
            addCriterion("location in", values, "location");
            return (Criteria) this;
        }

        public Criteria andLocationNotIn(List<String> values) {
            addCriterion("location not in", values, "location");
            return (Criteria) this;
        }

        public Criteria andLocationBetween(String value1, String value2) {
            addCriterion("location between", value1, value2, "location");
            return (Criteria) this;
        }

        public Criteria andLocationNotBetween(String value1, String value2) {
            addCriterion("location not between", value1, value2, "location");
            return (Criteria) this;
        }

        public Criteria andAddressIsNull() {
            addCriterion("address is null");
            return (Criteria) this;
        }

        public Criteria andAddressIsNotNull() {
            addCriterion("address is not null");
            return (Criteria) this;
        }

        public Criteria andAddressEqualTo(String value) {
            addCriterion("address =", value, "address");
            return (Criteria) this;
        }

        public Criteria andAddressNotEqualTo(String value) {
            addCriterion("address <>", value, "address");
            return (Criteria) this;
        }

        public Criteria andAddressGreaterThan(String value) {
            addCriterion("address >", value, "address");
            return (Criteria) this;
        }

        public Criteria andAddressGreaterThanOrEqualTo(String value) {
            addCriterion("address >=", value, "address");
            return (Criteria) this;
        }

        public Criteria andAddressLessThan(String value) {
            addCriterion("address <", value, "address");
            return (Criteria) this;
        }

        public Criteria andAddressLessThanOrEqualTo(String value) {
            addCriterion("address <=", value, "address");
            return (Criteria) this;
        }

        public Criteria andAddressLike(String value) {
            addCriterion("address like", value, "address");
            return (Criteria) this;
        }

        public Criteria andAddressNotLike(String value) {
            addCriterion("address not like", value, "address");
            return (Criteria) this;
        }

        public Criteria andAddressIn(List<String> values) {
            addCriterion("address in", values, "address");
            return (Criteria) this;
        }

        public Criteria andAddressNotIn(List<String> values) {
            addCriterion("address not in", values, "address");
            return (Criteria) this;
        }

        public Criteria andAddressBetween(String value1, String value2) {
            addCriterion("address between", value1, value2, "address");
            return (Criteria) this;
        }

        public Criteria andAddressNotBetween(String value1, String value2) {
            addCriterion("address not between", value1, value2, "address");
            return (Criteria) this;
        }

        public Criteria andLabelIsNull() {
            addCriterion("label is null");
            return (Criteria) this;
        }

        public Criteria andLabelIsNotNull() {
            addCriterion("label is not null");
            return (Criteria) this;
        }

        public Criteria andLabelEqualTo(String value) {
            addCriterion("label =", value, "label");
            return (Criteria) this;
        }

        public Criteria andLabelNotEqualTo(String value) {
            addCriterion("label <>", value, "label");
            return (Criteria) this;
        }

        public Criteria andLabelGreaterThan(String value) {
            addCriterion("label >", value, "label");
            return (Criteria) this;
        }

        public Criteria andLabelGreaterThanOrEqualTo(String value) {
            addCriterion("label >=", value, "label");
            return (Criteria) this;
        }

        public Criteria andLabelLessThan(String value) {
            addCriterion("label <", value, "label");
            return (Criteria) this;
        }

        public Criteria andLabelLessThanOrEqualTo(String value) {
            addCriterion("label <=", value, "label");
            return (Criteria) this;
        }

        public Criteria andLabelLike(String value) {
            addCriterion("label like", value, "label");
            return (Criteria) this;
        }

        public Criteria andLabelNotLike(String value) {
            addCriterion("label not like", value, "label");
            return (Criteria) this;
        }

        public Criteria andLabelIn(List<String> values) {
            addCriterion("label in", values, "label");
            return (Criteria) this;
        }

        public Criteria andLabelNotIn(List<String> values) {
            addCriterion("label not in", values, "label");
            return (Criteria) this;
        }

        public Criteria andLabelBetween(String value1, String value2) {
            addCriterion("label between", value1, value2, "label");
            return (Criteria) this;
        }

        public Criteria andLabelNotBetween(String value1, String value2) {
            addCriterion("label not between", value1, value2, "label");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNull() {
            addCriterion("create_time is null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNotNull() {
            addCriterion("create_time is not null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeEqualTo(Date value) {
            addCriterion("create_time =", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotEqualTo(Date value) {
            addCriterion("create_time <>", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThan(Date value) {
            addCriterion("create_time >", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("create_time >=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThan(Date value) {
            addCriterion("create_time <", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThanOrEqualTo(Date value) {
            addCriterion("create_time <=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIn(List<Date> values) {
            addCriterion("create_time in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotIn(List<Date> values) {
            addCriterion("create_time not in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeBetween(Date value1, Date value2) {
            addCriterion("create_time between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotBetween(Date value1, Date value2) {
            addCriterion("create_time not between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andBanner1IsNull() {
            addCriterion("banner1 is null");
            return (Criteria) this;
        }

        public Criteria andBanner1IsNotNull() {
            addCriterion("banner1 is not null");
            return (Criteria) this;
        }

        public Criteria andBanner1EqualTo(String value) {
            addCriterion("banner1 =", value, "banner1");
            return (Criteria) this;
        }

        public Criteria andBanner1NotEqualTo(String value) {
            addCriterion("banner1 <>", value, "banner1");
            return (Criteria) this;
        }

        public Criteria andBanner1GreaterThan(String value) {
            addCriterion("banner1 >", value, "banner1");
            return (Criteria) this;
        }

        public Criteria andBanner1GreaterThanOrEqualTo(String value) {
            addCriterion("banner1 >=", value, "banner1");
            return (Criteria) this;
        }

        public Criteria andBanner1LessThan(String value) {
            addCriterion("banner1 <", value, "banner1");
            return (Criteria) this;
        }

        public Criteria andBanner1LessThanOrEqualTo(String value) {
            addCriterion("banner1 <=", value, "banner1");
            return (Criteria) this;
        }

        public Criteria andBanner1Like(String value) {
            addCriterion("banner1 like", value, "banner1");
            return (Criteria) this;
        }

        public Criteria andBanner1NotLike(String value) {
            addCriterion("banner1 not like", value, "banner1");
            return (Criteria) this;
        }

        public Criteria andBanner1In(List<String> values) {
            addCriterion("banner1 in", values, "banner1");
            return (Criteria) this;
        }

        public Criteria andBanner1NotIn(List<String> values) {
            addCriterion("banner1 not in", values, "banner1");
            return (Criteria) this;
        }

        public Criteria andBanner1Between(String value1, String value2) {
            addCriterion("banner1 between", value1, value2, "banner1");
            return (Criteria) this;
        }

        public Criteria andBanner1NotBetween(String value1, String value2) {
            addCriterion("banner1 not between", value1, value2, "banner1");
            return (Criteria) this;
        }

        public Criteria andBanner2IsNull() {
            addCriterion("banner2 is null");
            return (Criteria) this;
        }

        public Criteria andBanner2IsNotNull() {
            addCriterion("banner2 is not null");
            return (Criteria) this;
        }

        public Criteria andBanner2EqualTo(String value) {
            addCriterion("banner2 =", value, "banner2");
            return (Criteria) this;
        }

        public Criteria andBanner2NotEqualTo(String value) {
            addCriterion("banner2 <>", value, "banner2");
            return (Criteria) this;
        }

        public Criteria andBanner2GreaterThan(String value) {
            addCriterion("banner2 >", value, "banner2");
            return (Criteria) this;
        }

        public Criteria andBanner2GreaterThanOrEqualTo(String value) {
            addCriterion("banner2 >=", value, "banner2");
            return (Criteria) this;
        }

        public Criteria andBanner2LessThan(String value) {
            addCriterion("banner2 <", value, "banner2");
            return (Criteria) this;
        }

        public Criteria andBanner2LessThanOrEqualTo(String value) {
            addCriterion("banner2 <=", value, "banner2");
            return (Criteria) this;
        }

        public Criteria andBanner2Like(String value) {
            addCriterion("banner2 like", value, "banner2");
            return (Criteria) this;
        }

        public Criteria andBanner2NotLike(String value) {
            addCriterion("banner2 not like", value, "banner2");
            return (Criteria) this;
        }

        public Criteria andBanner2In(List<String> values) {
            addCriterion("banner2 in", values, "banner2");
            return (Criteria) this;
        }

        public Criteria andBanner2NotIn(List<String> values) {
            addCriterion("banner2 not in", values, "banner2");
            return (Criteria) this;
        }

        public Criteria andBanner2Between(String value1, String value2) {
            addCriterion("banner2 between", value1, value2, "banner2");
            return (Criteria) this;
        }

        public Criteria andBanner2NotBetween(String value1, String value2) {
            addCriterion("banner2 not between", value1, value2, "banner2");
            return (Criteria) this;
        }

        public Criteria andBanner3IsNull() {
            addCriterion("banner3 is null");
            return (Criteria) this;
        }

        public Criteria andBanner3IsNotNull() {
            addCriterion("banner3 is not null");
            return (Criteria) this;
        }

        public Criteria andBanner3EqualTo(String value) {
            addCriterion("banner3 =", value, "banner3");
            return (Criteria) this;
        }

        public Criteria andBanner3NotEqualTo(String value) {
            addCriterion("banner3 <>", value, "banner3");
            return (Criteria) this;
        }

        public Criteria andBanner3GreaterThan(String value) {
            addCriterion("banner3 >", value, "banner3");
            return (Criteria) this;
        }

        public Criteria andBanner3GreaterThanOrEqualTo(String value) {
            addCriterion("banner3 >=", value, "banner3");
            return (Criteria) this;
        }

        public Criteria andBanner3LessThan(String value) {
            addCriterion("banner3 <", value, "banner3");
            return (Criteria) this;
        }

        public Criteria andBanner3LessThanOrEqualTo(String value) {
            addCriterion("banner3 <=", value, "banner3");
            return (Criteria) this;
        }

        public Criteria andBanner3Like(String value) {
            addCriterion("banner3 like", value, "banner3");
            return (Criteria) this;
        }

        public Criteria andBanner3NotLike(String value) {
            addCriterion("banner3 not like", value, "banner3");
            return (Criteria) this;
        }

        public Criteria andBanner3In(List<String> values) {
            addCriterion("banner3 in", values, "banner3");
            return (Criteria) this;
        }

        public Criteria andBanner3NotIn(List<String> values) {
            addCriterion("banner3 not in", values, "banner3");
            return (Criteria) this;
        }

        public Criteria andBanner3Between(String value1, String value2) {
            addCriterion("banner3 between", value1, value2, "banner3");
            return (Criteria) this;
        }

        public Criteria andBanner3NotBetween(String value1, String value2) {
            addCriterion("banner3 not between", value1, value2, "banner3");
            return (Criteria) this;
        }

        public Criteria andContactorIsNull() {
            addCriterion("contactor is null");
            return (Criteria) this;
        }

        public Criteria andContactorIsNotNull() {
            addCriterion("contactor is not null");
            return (Criteria) this;
        }

        public Criteria andContactorEqualTo(String value) {
            addCriterion("contactor =", value, "contactor");
            return (Criteria) this;
        }

        public Criteria andContactorNotEqualTo(String value) {
            addCriterion("contactor <>", value, "contactor");
            return (Criteria) this;
        }

        public Criteria andContactorGreaterThan(String value) {
            addCriterion("contactor >", value, "contactor");
            return (Criteria) this;
        }

        public Criteria andContactorGreaterThanOrEqualTo(String value) {
            addCriterion("contactor >=", value, "contactor");
            return (Criteria) this;
        }

        public Criteria andContactorLessThan(String value) {
            addCriterion("contactor <", value, "contactor");
            return (Criteria) this;
        }

        public Criteria andContactorLessThanOrEqualTo(String value) {
            addCriterion("contactor <=", value, "contactor");
            return (Criteria) this;
        }

        public Criteria andContactorLike(String value) {
            addCriterion("contactor like", value, "contactor");
            return (Criteria) this;
        }

        public Criteria andContactorNotLike(String value) {
            addCriterion("contactor not like", value, "contactor");
            return (Criteria) this;
        }

        public Criteria andContactorIn(List<String> values) {
            addCriterion("contactor in", values, "contactor");
            return (Criteria) this;
        }

        public Criteria andContactorNotIn(List<String> values) {
            addCriterion("contactor not in", values, "contactor");
            return (Criteria) this;
        }

        public Criteria andContactorBetween(String value1, String value2) {
            addCriterion("contactor between", value1, value2, "contactor");
            return (Criteria) this;
        }

        public Criteria andContactorNotBetween(String value1, String value2) {
            addCriterion("contactor not between", value1, value2, "contactor");
            return (Criteria) this;
        }

        public Criteria andPhoneIsNull() {
            addCriterion("phone is null");
            return (Criteria) this;
        }

        public Criteria andPhoneIsNotNull() {
            addCriterion("phone is not null");
            return (Criteria) this;
        }

        public Criteria andPhoneEqualTo(String value) {
            addCriterion("phone =", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneNotEqualTo(String value) {
            addCriterion("phone <>", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneGreaterThan(String value) {
            addCriterion("phone >", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneGreaterThanOrEqualTo(String value) {
            addCriterion("phone >=", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneLessThan(String value) {
            addCriterion("phone <", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneLessThanOrEqualTo(String value) {
            addCriterion("phone <=", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneLike(String value) {
            addCriterion("phone like", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneNotLike(String value) {
            addCriterion("phone not like", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneIn(List<String> values) {
            addCriterion("phone in", values, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneNotIn(List<String> values) {
            addCriterion("phone not in", values, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneBetween(String value1, String value2) {
            addCriterion("phone between", value1, value2, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneNotBetween(String value1, String value2) {
            addCriterion("phone not between", value1, value2, "phone");
            return (Criteria) this;
        }

        public Criteria andLatIsNull() {
            addCriterion("lat is null");
            return (Criteria) this;
        }

        public Criteria andLatIsNotNull() {
            addCriterion("lat is not null");
            return (Criteria) this;
        }

        public Criteria andLatEqualTo(String value) {
            addCriterion("lat =", value, "lat");
            return (Criteria) this;
        }

        public Criteria andLatNotEqualTo(String value) {
            addCriterion("lat <>", value, "lat");
            return (Criteria) this;
        }

        public Criteria andLatGreaterThan(String value) {
            addCriterion("lat >", value, "lat");
            return (Criteria) this;
        }

        public Criteria andLatGreaterThanOrEqualTo(String value) {
            addCriterion("lat >=", value, "lat");
            return (Criteria) this;
        }

        public Criteria andLatLessThan(String value) {
            addCriterion("lat <", value, "lat");
            return (Criteria) this;
        }

        public Criteria andLatLessThanOrEqualTo(String value) {
            addCriterion("lat <=", value, "lat");
            return (Criteria) this;
        }

        public Criteria andLatLike(String value) {
            addCriterion("lat like", value, "lat");
            return (Criteria) this;
        }

        public Criteria andLatNotLike(String value) {
            addCriterion("lat not like", value, "lat");
            return (Criteria) this;
        }

        public Criteria andLatIn(List<String> values) {
            addCriterion("lat in", values, "lat");
            return (Criteria) this;
        }

        public Criteria andLatNotIn(List<String> values) {
            addCriterion("lat not in", values, "lat");
            return (Criteria) this;
        }

        public Criteria andLatBetween(String value1, String value2) {
            addCriterion("lat between", value1, value2, "lat");
            return (Criteria) this;
        }

        public Criteria andLatNotBetween(String value1, String value2) {
            addCriterion("lat not between", value1, value2, "lat");
            return (Criteria) this;
        }

        public Criteria andLngIsNull() {
            addCriterion("lng is null");
            return (Criteria) this;
        }

        public Criteria andLngIsNotNull() {
            addCriterion("lng is not null");
            return (Criteria) this;
        }

        public Criteria andLngEqualTo(String value) {
            addCriterion("lng =", value, "lng");
            return (Criteria) this;
        }

        public Criteria andLngNotEqualTo(String value) {
            addCriterion("lng <>", value, "lng");
            return (Criteria) this;
        }

        public Criteria andLngGreaterThan(String value) {
            addCriterion("lng >", value, "lng");
            return (Criteria) this;
        }

        public Criteria andLngGreaterThanOrEqualTo(String value) {
            addCriterion("lng >=", value, "lng");
            return (Criteria) this;
        }

        public Criteria andLngLessThan(String value) {
            addCriterion("lng <", value, "lng");
            return (Criteria) this;
        }

        public Criteria andLngLessThanOrEqualTo(String value) {
            addCriterion("lng <=", value, "lng");
            return (Criteria) this;
        }

        public Criteria andLngLike(String value) {
            addCriterion("lng like", value, "lng");
            return (Criteria) this;
        }

        public Criteria andLngNotLike(String value) {
            addCriterion("lng not like", value, "lng");
            return (Criteria) this;
        }

        public Criteria andLngIn(List<String> values) {
            addCriterion("lng in", values, "lng");
            return (Criteria) this;
        }

        public Criteria andLngNotIn(List<String> values) {
            addCriterion("lng not in", values, "lng");
            return (Criteria) this;
        }

        public Criteria andLngBetween(String value1, String value2) {
            addCriterion("lng between", value1, value2, "lng");
            return (Criteria) this;
        }

        public Criteria andLngNotBetween(String value1, String value2) {
            addCriterion("lng not between", value1, value2, "lng");
            return (Criteria) this;
        }

        public Criteria andBanner4IsNull() {
            addCriterion("banner4 is null");
            return (Criteria) this;
        }

        public Criteria andBanner4IsNotNull() {
            addCriterion("banner4 is not null");
            return (Criteria) this;
        }

        public Criteria andBanner4EqualTo(String value) {
            addCriterion("banner4 =", value, "banner4");
            return (Criteria) this;
        }

        public Criteria andBanner4NotEqualTo(String value) {
            addCriterion("banner4 <>", value, "banner4");
            return (Criteria) this;
        }

        public Criteria andBanner4GreaterThan(String value) {
            addCriterion("banner4 >", value, "banner4");
            return (Criteria) this;
        }

        public Criteria andBanner4GreaterThanOrEqualTo(String value) {
            addCriterion("banner4 >=", value, "banner4");
            return (Criteria) this;
        }

        public Criteria andBanner4LessThan(String value) {
            addCriterion("banner4 <", value, "banner4");
            return (Criteria) this;
        }

        public Criteria andBanner4LessThanOrEqualTo(String value) {
            addCriterion("banner4 <=", value, "banner4");
            return (Criteria) this;
        }

        public Criteria andBanner4Like(String value) {
            addCriterion("banner4 like", value, "banner4");
            return (Criteria) this;
        }

        public Criteria andBanner4NotLike(String value) {
            addCriterion("banner4 not like", value, "banner4");
            return (Criteria) this;
        }

        public Criteria andBanner4In(List<String> values) {
            addCriterion("banner4 in", values, "banner4");
            return (Criteria) this;
        }

        public Criteria andBanner4NotIn(List<String> values) {
            addCriterion("banner4 not in", values, "banner4");
            return (Criteria) this;
        }

        public Criteria andBanner4Between(String value1, String value2) {
            addCriterion("banner4 between", value1, value2, "banner4");
            return (Criteria) this;
        }

        public Criteria andBanner4NotBetween(String value1, String value2) {
            addCriterion("banner4 not between", value1, value2, "banner4");
            return (Criteria) this;
        }

        public Criteria andBanner5IsNull() {
            addCriterion("banner5 is null");
            return (Criteria) this;
        }

        public Criteria andBanner5IsNotNull() {
            addCriterion("banner5 is not null");
            return (Criteria) this;
        }

        public Criteria andBanner5EqualTo(String value) {
            addCriterion("banner5 =", value, "banner5");
            return (Criteria) this;
        }

        public Criteria andBanner5NotEqualTo(String value) {
            addCriterion("banner5 <>", value, "banner5");
            return (Criteria) this;
        }

        public Criteria andBanner5GreaterThan(String value) {
            addCriterion("banner5 >", value, "banner5");
            return (Criteria) this;
        }

        public Criteria andBanner5GreaterThanOrEqualTo(String value) {
            addCriterion("banner5 >=", value, "banner5");
            return (Criteria) this;
        }

        public Criteria andBanner5LessThan(String value) {
            addCriterion("banner5 <", value, "banner5");
            return (Criteria) this;
        }

        public Criteria andBanner5LessThanOrEqualTo(String value) {
            addCriterion("banner5 <=", value, "banner5");
            return (Criteria) this;
        }

        public Criteria andBanner5Like(String value) {
            addCriterion("banner5 like", value, "banner5");
            return (Criteria) this;
        }

        public Criteria andBanner5NotLike(String value) {
            addCriterion("banner5 not like", value, "banner5");
            return (Criteria) this;
        }

        public Criteria andBanner5In(List<String> values) {
            addCriterion("banner5 in", values, "banner5");
            return (Criteria) this;
        }

        public Criteria andBanner5NotIn(List<String> values) {
            addCriterion("banner5 not in", values, "banner5");
            return (Criteria) this;
        }

        public Criteria andBanner5Between(String value1, String value2) {
            addCriterion("banner5 between", value1, value2, "banner5");
            return (Criteria) this;
        }

        public Criteria andBanner5NotBetween(String value1, String value2) {
            addCriterion("banner5 not between", value1, value2, "banner5");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}