package com.carl.guli.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class GoodExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public GoodExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andGoodIdIsNull() {
            addCriterion("good_id is null");
            return (Criteria) this;
        }

        public Criteria andGoodIdIsNotNull() {
            addCriterion("good_id is not null");
            return (Criteria) this;
        }

        public Criteria andGoodIdEqualTo(Integer value) {
            addCriterion("good_id =", value, "goodId");
            return (Criteria) this;
        }

        public Criteria andGoodIdNotEqualTo(Integer value) {
            addCriterion("good_id <>", value, "goodId");
            return (Criteria) this;
        }

        public Criteria andGoodIdGreaterThan(Integer value) {
            addCriterion("good_id >", value, "goodId");
            return (Criteria) this;
        }

        public Criteria andGoodIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("good_id >=", value, "goodId");
            return (Criteria) this;
        }

        public Criteria andGoodIdLessThan(Integer value) {
            addCriterion("good_id <", value, "goodId");
            return (Criteria) this;
        }

        public Criteria andGoodIdLessThanOrEqualTo(Integer value) {
            addCriterion("good_id <=", value, "goodId");
            return (Criteria) this;
        }

        public Criteria andGoodIdIn(List<Integer> values) {
            addCriterion("good_id in", values, "goodId");
            return (Criteria) this;
        }

        public Criteria andGoodIdNotIn(List<Integer> values) {
            addCriterion("good_id not in", values, "goodId");
            return (Criteria) this;
        }

        public Criteria andGoodIdBetween(Integer value1, Integer value2) {
            addCriterion("good_id between", value1, value2, "goodId");
            return (Criteria) this;
        }

        public Criteria andGoodIdNotBetween(Integer value1, Integer value2) {
            addCriterion("good_id not between", value1, value2, "goodId");
            return (Criteria) this;
        }

        public Criteria andNameIsNull() {
            addCriterion("name is null");
            return (Criteria) this;
        }

        public Criteria andNameIsNotNull() {
            addCriterion("name is not null");
            return (Criteria) this;
        }

        public Criteria andNameEqualTo(String value) {
            addCriterion("name =", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotEqualTo(String value) {
            addCriterion("name <>", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameGreaterThan(String value) {
            addCriterion("name >", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameGreaterThanOrEqualTo(String value) {
            addCriterion("name >=", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLessThan(String value) {
            addCriterion("name <", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLessThanOrEqualTo(String value) {
            addCriterion("name <=", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLike(String value) {
            addCriterion("name like", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotLike(String value) {
            addCriterion("name not like", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameIn(List<String> values) {
            addCriterion("name in", values, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotIn(List<String> values) {
            addCriterion("name not in", values, "name");
            return (Criteria) this;
        }

        public Criteria andNameBetween(String value1, String value2) {
            addCriterion("name between", value1, value2, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotBetween(String value1, String value2) {
            addCriterion("name not between", value1, value2, "name");
            return (Criteria) this;
        }

        public Criteria andGoodPicIsNull() {
            addCriterion("good_pic is null");
            return (Criteria) this;
        }

        public Criteria andGoodPicIsNotNull() {
            addCriterion("good_pic is not null");
            return (Criteria) this;
        }

        public Criteria andGoodPicEqualTo(String value) {
            addCriterion("good_pic =", value, "goodPic");
            return (Criteria) this;
        }

        public Criteria andGoodPicNotEqualTo(String value) {
            addCriterion("good_pic <>", value, "goodPic");
            return (Criteria) this;
        }

        public Criteria andGoodPicGreaterThan(String value) {
            addCriterion("good_pic >", value, "goodPic");
            return (Criteria) this;
        }

        public Criteria andGoodPicGreaterThanOrEqualTo(String value) {
            addCriterion("good_pic >=", value, "goodPic");
            return (Criteria) this;
        }

        public Criteria andGoodPicLessThan(String value) {
            addCriterion("good_pic <", value, "goodPic");
            return (Criteria) this;
        }

        public Criteria andGoodPicLessThanOrEqualTo(String value) {
            addCriterion("good_pic <=", value, "goodPic");
            return (Criteria) this;
        }

        public Criteria andGoodPicLike(String value) {
            addCriterion("good_pic like", value, "goodPic");
            return (Criteria) this;
        }

        public Criteria andGoodPicNotLike(String value) {
            addCriterion("good_pic not like", value, "goodPic");
            return (Criteria) this;
        }

        public Criteria andGoodPicIn(List<String> values) {
            addCriterion("good_pic in", values, "goodPic");
            return (Criteria) this;
        }

        public Criteria andGoodPicNotIn(List<String> values) {
            addCriterion("good_pic not in", values, "goodPic");
            return (Criteria) this;
        }

        public Criteria andGoodPicBetween(String value1, String value2) {
            addCriterion("good_pic between", value1, value2, "goodPic");
            return (Criteria) this;
        }

        public Criteria andGoodPicNotBetween(String value1, String value2) {
            addCriterion("good_pic not between", value1, value2, "goodPic");
            return (Criteria) this;
        }

        public Criteria andPriceIsNull() {
            addCriterion("price is null");
            return (Criteria) this;
        }

        public Criteria andPriceIsNotNull() {
            addCriterion("price is not null");
            return (Criteria) this;
        }

        public Criteria andPriceEqualTo(Float value) {
            addCriterion("price =", value, "price");
            return (Criteria) this;
        }

        public Criteria andPriceNotEqualTo(Float value) {
            addCriterion("price <>", value, "price");
            return (Criteria) this;
        }

        public Criteria andPriceGreaterThan(Float value) {
            addCriterion("price >", value, "price");
            return (Criteria) this;
        }

        public Criteria andPriceGreaterThanOrEqualTo(Float value) {
            addCriterion("price >=", value, "price");
            return (Criteria) this;
        }

        public Criteria andPriceLessThan(Float value) {
            addCriterion("price <", value, "price");
            return (Criteria) this;
        }

        public Criteria andPriceLessThanOrEqualTo(Float value) {
            addCriterion("price <=", value, "price");
            return (Criteria) this;
        }

        public Criteria andPriceIn(List<Float> values) {
            addCriterion("price in", values, "price");
            return (Criteria) this;
        }

        public Criteria andPriceNotIn(List<Float> values) {
            addCriterion("price not in", values, "price");
            return (Criteria) this;
        }

        public Criteria andPriceBetween(Float value1, Float value2) {
            addCriterion("price between", value1, value2, "price");
            return (Criteria) this;
        }

        public Criteria andPriceNotBetween(Float value1, Float value2) {
            addCriterion("price not between", value1, value2, "price");
            return (Criteria) this;
        }

        public Criteria andLocationIsNull() {
            addCriterion("location is null");
            return (Criteria) this;
        }

        public Criteria andLocationIsNotNull() {
            addCriterion("location is not null");
            return (Criteria) this;
        }

        public Criteria andLocationEqualTo(String value) {
            addCriterion("location =", value, "location");
            return (Criteria) this;
        }

        public Criteria andLocationNotEqualTo(String value) {
            addCriterion("location <>", value, "location");
            return (Criteria) this;
        }

        public Criteria andLocationGreaterThan(String value) {
            addCriterion("location >", value, "location");
            return (Criteria) this;
        }

        public Criteria andLocationGreaterThanOrEqualTo(String value) {
            addCriterion("location >=", value, "location");
            return (Criteria) this;
        }

        public Criteria andLocationLessThan(String value) {
            addCriterion("location <", value, "location");
            return (Criteria) this;
        }

        public Criteria andLocationLessThanOrEqualTo(String value) {
            addCriterion("location <=", value, "location");
            return (Criteria) this;
        }

        public Criteria andLocationLike(String value) {
            addCriterion("location like", value, "location");
            return (Criteria) this;
        }

        public Criteria andLocationNotLike(String value) {
            addCriterion("location not like", value, "location");
            return (Criteria) this;
        }

        public Criteria andLocationIn(List<String> values) {
            addCriterion("location in", values, "location");
            return (Criteria) this;
        }

        public Criteria andLocationNotIn(List<String> values) {
            addCriterion("location not in", values, "location");
            return (Criteria) this;
        }

        public Criteria andLocationBetween(String value1, String value2) {
            addCriterion("location between", value1, value2, "location");
            return (Criteria) this;
        }

        public Criteria andLocationNotBetween(String value1, String value2) {
            addCriterion("location not between", value1, value2, "location");
            return (Criteria) this;
        }

        public Criteria andAddressIsNull() {
            addCriterion("address is null");
            return (Criteria) this;
        }

        public Criteria andAddressIsNotNull() {
            addCriterion("address is not null");
            return (Criteria) this;
        }

        public Criteria andAddressEqualTo(String value) {
            addCriterion("address =", value, "address");
            return (Criteria) this;
        }

        public Criteria andAddressNotEqualTo(String value) {
            addCriterion("address <>", value, "address");
            return (Criteria) this;
        }

        public Criteria andAddressGreaterThan(String value) {
            addCriterion("address >", value, "address");
            return (Criteria) this;
        }

        public Criteria andAddressGreaterThanOrEqualTo(String value) {
            addCriterion("address >=", value, "address");
            return (Criteria) this;
        }

        public Criteria andAddressLessThan(String value) {
            addCriterion("address <", value, "address");
            return (Criteria) this;
        }

        public Criteria andAddressLessThanOrEqualTo(String value) {
            addCriterion("address <=", value, "address");
            return (Criteria) this;
        }

        public Criteria andAddressLike(String value) {
            addCriterion("address like", value, "address");
            return (Criteria) this;
        }

        public Criteria andAddressNotLike(String value) {
            addCriterion("address not like", value, "address");
            return (Criteria) this;
        }

        public Criteria andAddressIn(List<String> values) {
            addCriterion("address in", values, "address");
            return (Criteria) this;
        }

        public Criteria andAddressNotIn(List<String> values) {
            addCriterion("address not in", values, "address");
            return (Criteria) this;
        }

        public Criteria andAddressBetween(String value1, String value2) {
            addCriterion("address between", value1, value2, "address");
            return (Criteria) this;
        }

        public Criteria andAddressNotBetween(String value1, String value2) {
            addCriterion("address not between", value1, value2, "address");
            return (Criteria) this;
        }

        public Criteria andLabelIsNull() {
            addCriterion("label is null");
            return (Criteria) this;
        }

        public Criteria andLabelIsNotNull() {
            addCriterion("label is not null");
            return (Criteria) this;
        }

        public Criteria andLabelEqualTo(String value) {
            addCriterion("label =", value, "label");
            return (Criteria) this;
        }

        public Criteria andLabelNotEqualTo(String value) {
            addCriterion("label <>", value, "label");
            return (Criteria) this;
        }

        public Criteria andLabelGreaterThan(String value) {
            addCriterion("label >", value, "label");
            return (Criteria) this;
        }

        public Criteria andLabelGreaterThanOrEqualTo(String value) {
            addCriterion("label >=", value, "label");
            return (Criteria) this;
        }

        public Criteria andLabelLessThan(String value) {
            addCriterion("label <", value, "label");
            return (Criteria) this;
        }

        public Criteria andLabelLessThanOrEqualTo(String value) {
            addCriterion("label <=", value, "label");
            return (Criteria) this;
        }

        public Criteria andLabelLike(String value) {
            addCriterion("label like", value, "label");
            return (Criteria) this;
        }

        public Criteria andLabelNotLike(String value) {
            addCriterion("label not like", value, "label");
            return (Criteria) this;
        }

        public Criteria andLabelIn(List<String> values) {
            addCriterion("label in", values, "label");
            return (Criteria) this;
        }

        public Criteria andLabelNotIn(List<String> values) {
            addCriterion("label not in", values, "label");
            return (Criteria) this;
        }

        public Criteria andLabelBetween(String value1, String value2) {
            addCriterion("label between", value1, value2, "label");
            return (Criteria) this;
        }

        public Criteria andLabelNotBetween(String value1, String value2) {
            addCriterion("label not between", value1, value2, "label");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNull() {
            addCriterion("create_time is null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNotNull() {
            addCriterion("create_time is not null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeEqualTo(Date value) {
            addCriterion("create_time =", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotEqualTo(Date value) {
            addCriterion("create_time <>", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThan(Date value) {
            addCriterion("create_time >", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("create_time >=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThan(Date value) {
            addCriterion("create_time <", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThanOrEqualTo(Date value) {
            addCriterion("create_time <=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIn(List<Date> values) {
            addCriterion("create_time in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotIn(List<Date> values) {
            addCriterion("create_time not in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeBetween(Date value1, Date value2) {
            addCriterion("create_time between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotBetween(Date value1, Date value2) {
            addCriterion("create_time not between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andBanner1IsNull() {
            addCriterion("banner1 is null");
            return (Criteria) this;
        }

        public Criteria andBanner1IsNotNull() {
            addCriterion("banner1 is not null");
            return (Criteria) this;
        }

        public Criteria andBanner1EqualTo(String value) {
            addCriterion("banner1 =", value, "banner1");
            return (Criteria) this;
        }

        public Criteria andBanner1NotEqualTo(String value) {
            addCriterion("banner1 <>", value, "banner1");
            return (Criteria) this;
        }

        public Criteria andBanner1GreaterThan(String value) {
            addCriterion("banner1 >", value, "banner1");
            return (Criteria) this;
        }

        public Criteria andBanner1GreaterThanOrEqualTo(String value) {
            addCriterion("banner1 >=", value, "banner1");
            return (Criteria) this;
        }

        public Criteria andBanner1LessThan(String value) {
            addCriterion("banner1 <", value, "banner1");
            return (Criteria) this;
        }

        public Criteria andBanner1LessThanOrEqualTo(String value) {
            addCriterion("banner1 <=", value, "banner1");
            return (Criteria) this;
        }

        public Criteria andBanner1Like(String value) {
            addCriterion("banner1 like", value, "banner1");
            return (Criteria) this;
        }

        public Criteria andBanner1NotLike(String value) {
            addCriterion("banner1 not like", value, "banner1");
            return (Criteria) this;
        }

        public Criteria andBanner1In(List<String> values) {
            addCriterion("banner1 in", values, "banner1");
            return (Criteria) this;
        }

        public Criteria andBanner1NotIn(List<String> values) {
            addCriterion("banner1 not in", values, "banner1");
            return (Criteria) this;
        }

        public Criteria andBanner1Between(String value1, String value2) {
            addCriterion("banner1 between", value1, value2, "banner1");
            return (Criteria) this;
        }

        public Criteria andBanner1NotBetween(String value1, String value2) {
            addCriterion("banner1 not between", value1, value2, "banner1");
            return (Criteria) this;
        }

        public Criteria andBanner2IsNull() {
            addCriterion("banner2 is null");
            return (Criteria) this;
        }

        public Criteria andBanner2IsNotNull() {
            addCriterion("banner2 is not null");
            return (Criteria) this;
        }

        public Criteria andBanner2EqualTo(String value) {
            addCriterion("banner2 =", value, "banner2");
            return (Criteria) this;
        }

        public Criteria andBanner2NotEqualTo(String value) {
            addCriterion("banner2 <>", value, "banner2");
            return (Criteria) this;
        }

        public Criteria andBanner2GreaterThan(String value) {
            addCriterion("banner2 >", value, "banner2");
            return (Criteria) this;
        }

        public Criteria andBanner2GreaterThanOrEqualTo(String value) {
            addCriterion("banner2 >=", value, "banner2");
            return (Criteria) this;
        }

        public Criteria andBanner2LessThan(String value) {
            addCriterion("banner2 <", value, "banner2");
            return (Criteria) this;
        }

        public Criteria andBanner2LessThanOrEqualTo(String value) {
            addCriterion("banner2 <=", value, "banner2");
            return (Criteria) this;
        }

        public Criteria andBanner2Like(String value) {
            addCriterion("banner2 like", value, "banner2");
            return (Criteria) this;
        }

        public Criteria andBanner2NotLike(String value) {
            addCriterion("banner2 not like", value, "banner2");
            return (Criteria) this;
        }

        public Criteria andBanner2In(List<String> values) {
            addCriterion("banner2 in", values, "banner2");
            return (Criteria) this;
        }

        public Criteria andBanner2NotIn(List<String> values) {
            addCriterion("banner2 not in", values, "banner2");
            return (Criteria) this;
        }

        public Criteria andBanner2Between(String value1, String value2) {
            addCriterion("banner2 between", value1, value2, "banner2");
            return (Criteria) this;
        }

        public Criteria andBanner2NotBetween(String value1, String value2) {
            addCriterion("banner2 not between", value1, value2, "banner2");
            return (Criteria) this;
        }

        public Criteria andBanner3IsNull() {
            addCriterion("banner3 is null");
            return (Criteria) this;
        }

        public Criteria andBanner3IsNotNull() {
            addCriterion("banner3 is not null");
            return (Criteria) this;
        }

        public Criteria andBanner3EqualTo(String value) {
            addCriterion("banner3 =", value, "banner3");
            return (Criteria) this;
        }

        public Criteria andBanner3NotEqualTo(String value) {
            addCriterion("banner3 <>", value, "banner3");
            return (Criteria) this;
        }

        public Criteria andBanner3GreaterThan(String value) {
            addCriterion("banner3 >", value, "banner3");
            return (Criteria) this;
        }

        public Criteria andBanner3GreaterThanOrEqualTo(String value) {
            addCriterion("banner3 >=", value, "banner3");
            return (Criteria) this;
        }

        public Criteria andBanner3LessThan(String value) {
            addCriterion("banner3 <", value, "banner3");
            return (Criteria) this;
        }

        public Criteria andBanner3LessThanOrEqualTo(String value) {
            addCriterion("banner3 <=", value, "banner3");
            return (Criteria) this;
        }

        public Criteria andBanner3Like(String value) {
            addCriterion("banner3 like", value, "banner3");
            return (Criteria) this;
        }

        public Criteria andBanner3NotLike(String value) {
            addCriterion("banner3 not like", value, "banner3");
            return (Criteria) this;
        }

        public Criteria andBanner3In(List<String> values) {
            addCriterion("banner3 in", values, "banner3");
            return (Criteria) this;
        }

        public Criteria andBanner3NotIn(List<String> values) {
            addCriterion("banner3 not in", values, "banner3");
            return (Criteria) this;
        }

        public Criteria andBanner3Between(String value1, String value2) {
            addCriterion("banner3 between", value1, value2, "banner3");
            return (Criteria) this;
        }

        public Criteria andBanner3NotBetween(String value1, String value2) {
            addCriterion("banner3 not between", value1, value2, "banner3");
            return (Criteria) this;
        }

        public Criteria andContactorIsNull() {
            addCriterion("contactor is null");
            return (Criteria) this;
        }

        public Criteria andContactorIsNotNull() {
            addCriterion("contactor is not null");
            return (Criteria) this;
        }

        public Criteria andContactorEqualTo(String value) {
            addCriterion("contactor =", value, "contactor");
            return (Criteria) this;
        }

        public Criteria andContactorNotEqualTo(String value) {
            addCriterion("contactor <>", value, "contactor");
            return (Criteria) this;
        }

        public Criteria andContactorGreaterThan(String value) {
            addCriterion("contactor >", value, "contactor");
            return (Criteria) this;
        }

        public Criteria andContactorGreaterThanOrEqualTo(String value) {
            addCriterion("contactor >=", value, "contactor");
            return (Criteria) this;
        }

        public Criteria andContactorLessThan(String value) {
            addCriterion("contactor <", value, "contactor");
            return (Criteria) this;
        }

        public Criteria andContactorLessThanOrEqualTo(String value) {
            addCriterion("contactor <=", value, "contactor");
            return (Criteria) this;
        }

        public Criteria andContactorLike(String value) {
            addCriterion("contactor like", value, "contactor");
            return (Criteria) this;
        }

        public Criteria andContactorNotLike(String value) {
            addCriterion("contactor not like", value, "contactor");
            return (Criteria) this;
        }

        public Criteria andContactorIn(List<String> values) {
            addCriterion("contactor in", values, "contactor");
            return (Criteria) this;
        }

        public Criteria andContactorNotIn(List<String> values) {
            addCriterion("contactor not in", values, "contactor");
            return (Criteria) this;
        }

        public Criteria andContactorBetween(String value1, String value2) {
            addCriterion("contactor between", value1, value2, "contactor");
            return (Criteria) this;
        }

        public Criteria andContactorNotBetween(String value1, String value2) {
            addCriterion("contactor not between", value1, value2, "contactor");
            return (Criteria) this;
        }

        public Criteria andPhoneIsNull() {
            addCriterion("phone is null");
            return (Criteria) this;
        }

        public Criteria andPhoneIsNotNull() {
            addCriterion("phone is not null");
            return (Criteria) this;
        }

        public Criteria andPhoneEqualTo(String value) {
            addCriterion("phone =", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneNotEqualTo(String value) {
            addCriterion("phone <>", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneGreaterThan(String value) {
            addCriterion("phone >", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneGreaterThanOrEqualTo(String value) {
            addCriterion("phone >=", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneLessThan(String value) {
            addCriterion("phone <", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneLessThanOrEqualTo(String value) {
            addCriterion("phone <=", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneLike(String value) {
            addCriterion("phone like", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneNotLike(String value) {
            addCriterion("phone not like", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneIn(List<String> values) {
            addCriterion("phone in", values, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneNotIn(List<String> values) {
            addCriterion("phone not in", values, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneBetween(String value1, String value2) {
            addCriterion("phone between", value1, value2, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneNotBetween(String value1, String value2) {
            addCriterion("phone not between", value1, value2, "phone");
            return (Criteria) this;
        }

        public Criteria andIntroIsNull() {
            addCriterion("intro is null");
            return (Criteria) this;
        }

        public Criteria andIntroIsNotNull() {
            addCriterion("intro is not null");
            return (Criteria) this;
        }

        public Criteria andIntroEqualTo(String value) {
            addCriterion("intro =", value, "intro");
            return (Criteria) this;
        }

        public Criteria andIntroNotEqualTo(String value) {
            addCriterion("intro <>", value, "intro");
            return (Criteria) this;
        }

        public Criteria andIntroGreaterThan(String value) {
            addCriterion("intro >", value, "intro");
            return (Criteria) this;
        }

        public Criteria andIntroGreaterThanOrEqualTo(String value) {
            addCriterion("intro >=", value, "intro");
            return (Criteria) this;
        }

        public Criteria andIntroLessThan(String value) {
            addCriterion("intro <", value, "intro");
            return (Criteria) this;
        }

        public Criteria andIntroLessThanOrEqualTo(String value) {
            addCriterion("intro <=", value, "intro");
            return (Criteria) this;
        }

        public Criteria andIntroLike(String value) {
            addCriterion("intro like", value, "intro");
            return (Criteria) this;
        }

        public Criteria andIntroNotLike(String value) {
            addCriterion("intro not like", value, "intro");
            return (Criteria) this;
        }

        public Criteria andIntroIn(List<String> values) {
            addCriterion("intro in", values, "intro");
            return (Criteria) this;
        }

        public Criteria andIntroNotIn(List<String> values) {
            addCriterion("intro not in", values, "intro");
            return (Criteria) this;
        }

        public Criteria andIntroBetween(String value1, String value2) {
            addCriterion("intro between", value1, value2, "intro");
            return (Criteria) this;
        }

        public Criteria andIntroNotBetween(String value1, String value2) {
            addCriterion("intro not between", value1, value2, "intro");
            return (Criteria) this;
        }

        public Criteria andUnitIsNull() {
            addCriterion("unit is null");
            return (Criteria) this;
        }

        public Criteria andUnitIsNotNull() {
            addCriterion("unit is not null");
            return (Criteria) this;
        }

        public Criteria andUnitEqualTo(String value) {
            addCriterion("unit =", value, "unit");
            return (Criteria) this;
        }

        public Criteria andUnitNotEqualTo(String value) {
            addCriterion("unit <>", value, "unit");
            return (Criteria) this;
        }

        public Criteria andUnitGreaterThan(String value) {
            addCriterion("unit >", value, "unit");
            return (Criteria) this;
        }

        public Criteria andUnitGreaterThanOrEqualTo(String value) {
            addCriterion("unit >=", value, "unit");
            return (Criteria) this;
        }

        public Criteria andUnitLessThan(String value) {
            addCriterion("unit <", value, "unit");
            return (Criteria) this;
        }

        public Criteria andUnitLessThanOrEqualTo(String value) {
            addCriterion("unit <=", value, "unit");
            return (Criteria) this;
        }

        public Criteria andUnitLike(String value) {
            addCriterion("unit like", value, "unit");
            return (Criteria) this;
        }

        public Criteria andUnitNotLike(String value) {
            addCriterion("unit not like", value, "unit");
            return (Criteria) this;
        }

        public Criteria andUnitIn(List<String> values) {
            addCriterion("unit in", values, "unit");
            return (Criteria) this;
        }

        public Criteria andUnitNotIn(List<String> values) {
            addCriterion("unit not in", values, "unit");
            return (Criteria) this;
        }

        public Criteria andUnitBetween(String value1, String value2) {
            addCriterion("unit between", value1, value2, "unit");
            return (Criteria) this;
        }

        public Criteria andUnitNotBetween(String value1, String value2) {
            addCriterion("unit not between", value1, value2, "unit");
            return (Criteria) this;
        }

        public Criteria andSortIsNull() {
            addCriterion("sort is null");
            return (Criteria) this;
        }

        public Criteria andSortIsNotNull() {
            addCriterion("sort is not null");
            return (Criteria) this;
        }

        public Criteria andSortEqualTo(Integer value) {
            addCriterion("sort =", value, "sort");
            return (Criteria) this;
        }

        public Criteria andSortNotEqualTo(Integer value) {
            addCriterion("sort <>", value, "sort");
            return (Criteria) this;
        }

        public Criteria andSortGreaterThan(Integer value) {
            addCriterion("sort >", value, "sort");
            return (Criteria) this;
        }

        public Criteria andSortGreaterThanOrEqualTo(Integer value) {
            addCriterion("sort >=", value, "sort");
            return (Criteria) this;
        }

        public Criteria andSortLessThan(Integer value) {
            addCriterion("sort <", value, "sort");
            return (Criteria) this;
        }

        public Criteria andSortLessThanOrEqualTo(Integer value) {
            addCriterion("sort <=", value, "sort");
            return (Criteria) this;
        }

        public Criteria andSortIn(List<Integer> values) {
            addCriterion("sort in", values, "sort");
            return (Criteria) this;
        }

        public Criteria andSortNotIn(List<Integer> values) {
            addCriterion("sort not in", values, "sort");
            return (Criteria) this;
        }

        public Criteria andSortBetween(Integer value1, Integer value2) {
            addCriterion("sort between", value1, value2, "sort");
            return (Criteria) this;
        }

        public Criteria andSortNotBetween(Integer value1, Integer value2) {
            addCriterion("sort not between", value1, value2, "sort");
            return (Criteria) this;
        }

        public Criteria andBanner4IsNull() {
            addCriterion("banner4 is null");
            return (Criteria) this;
        }

        public Criteria andBanner4IsNotNull() {
            addCriterion("banner4 is not null");
            return (Criteria) this;
        }

        public Criteria andBanner4EqualTo(String value) {
            addCriterion("banner4 =", value, "banner4");
            return (Criteria) this;
        }

        public Criteria andBanner4NotEqualTo(String value) {
            addCriterion("banner4 <>", value, "banner4");
            return (Criteria) this;
        }

        public Criteria andBanner4GreaterThan(String value) {
            addCriterion("banner4 >", value, "banner4");
            return (Criteria) this;
        }

        public Criteria andBanner4GreaterThanOrEqualTo(String value) {
            addCriterion("banner4 >=", value, "banner4");
            return (Criteria) this;
        }

        public Criteria andBanner4LessThan(String value) {
            addCriterion("banner4 <", value, "banner4");
            return (Criteria) this;
        }

        public Criteria andBanner4LessThanOrEqualTo(String value) {
            addCriterion("banner4 <=", value, "banner4");
            return (Criteria) this;
        }

        public Criteria andBanner4Like(String value) {
            addCriterion("banner4 like", value, "banner4");
            return (Criteria) this;
        }

        public Criteria andBanner4NotLike(String value) {
            addCriterion("banner4 not like", value, "banner4");
            return (Criteria) this;
        }

        public Criteria andBanner4In(List<String> values) {
            addCriterion("banner4 in", values, "banner4");
            return (Criteria) this;
        }

        public Criteria andBanner4NotIn(List<String> values) {
            addCriterion("banner4 not in", values, "banner4");
            return (Criteria) this;
        }

        public Criteria andBanner4Between(String value1, String value2) {
            addCriterion("banner4 between", value1, value2, "banner4");
            return (Criteria) this;
        }

        public Criteria andBanner4NotBetween(String value1, String value2) {
            addCriterion("banner4 not between", value1, value2, "banner4");
            return (Criteria) this;
        }

        public Criteria andBanner5IsNull() {
            addCriterion("banner5 is null");
            return (Criteria) this;
        }

        public Criteria andBanner5IsNotNull() {
            addCriterion("banner5 is not null");
            return (Criteria) this;
        }

        public Criteria andBanner5EqualTo(String value) {
            addCriterion("banner5 =", value, "banner5");
            return (Criteria) this;
        }

        public Criteria andBanner5NotEqualTo(String value) {
            addCriterion("banner5 <>", value, "banner5");
            return (Criteria) this;
        }

        public Criteria andBanner5GreaterThan(String value) {
            addCriterion("banner5 >", value, "banner5");
            return (Criteria) this;
        }

        public Criteria andBanner5GreaterThanOrEqualTo(String value) {
            addCriterion("banner5 >=", value, "banner5");
            return (Criteria) this;
        }

        public Criteria andBanner5LessThan(String value) {
            addCriterion("banner5 <", value, "banner5");
            return (Criteria) this;
        }

        public Criteria andBanner5LessThanOrEqualTo(String value) {
            addCriterion("banner5 <=", value, "banner5");
            return (Criteria) this;
        }

        public Criteria andBanner5Like(String value) {
            addCriterion("banner5 like", value, "banner5");
            return (Criteria) this;
        }

        public Criteria andBanner5NotLike(String value) {
            addCriterion("banner5 not like", value, "banner5");
            return (Criteria) this;
        }

        public Criteria andBanner5In(List<String> values) {
            addCriterion("banner5 in", values, "banner5");
            return (Criteria) this;
        }

        public Criteria andBanner5NotIn(List<String> values) {
            addCriterion("banner5 not in", values, "banner5");
            return (Criteria) this;
        }

        public Criteria andBanner5Between(String value1, String value2) {
            addCriterion("banner5 between", value1, value2, "banner5");
            return (Criteria) this;
        }

        public Criteria andBanner5NotBetween(String value1, String value2) {
            addCriterion("banner5 not between", value1, value2, "banner5");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}