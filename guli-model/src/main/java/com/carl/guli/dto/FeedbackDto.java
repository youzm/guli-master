package com.carl.guli.dto;

import com.carl.guli.dto.common.Page;
import com.carl.guli.model.Feedback;

/**
 * Created by youzm on 2017/5/11.
 */
public class FeedbackDto extends Feedback{

    private Page page;

    public Page getPage() {
        return page;
    }

    public void setPage(Page page) {
        this.page = page;
    }
}
