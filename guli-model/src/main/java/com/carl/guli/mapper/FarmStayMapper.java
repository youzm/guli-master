package com.carl.guli.mapper;

import com.carl.guli.model.FarmStay;
import com.carl.guli.model.FarmStayExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface FarmStayMapper {
    int countByExample(FarmStayExample example);

    int deleteByExample(FarmStayExample example);

    int deleteByPrimaryKey(Integer farmId);

    int insert(FarmStay record);

    int insertSelective(FarmStay record);

    List<FarmStay> selectByExampleWithBLOBs(FarmStayExample example);

    List<FarmStay> selectByExample(FarmStayExample example);

    FarmStay selectByPrimaryKey(Integer farmId);

    int updateByExampleSelective(@Param("record") FarmStay record, @Param("example") FarmStayExample example);

    int updateByExampleWithBLOBs(@Param("record") FarmStay record, @Param("example") FarmStayExample example);

    int updateByExample(@Param("record") FarmStay record, @Param("example") FarmStayExample example);

    int updateByPrimaryKeySelective(FarmStay record);

    int updateByPrimaryKeyWithBLOBs(FarmStay record);

    int updateByPrimaryKey(FarmStay record);
}