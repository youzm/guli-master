package com.carl.guli.mapper;

import com.carl.guli.model.ScenicSpot;
import com.carl.guli.model.ScenicSpotExample;
import com.carl.guli.model.ScenicSpotWithBLOBs;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface ScenicSpotMapper {
    int countByExample(ScenicSpotExample example);

    int deleteByExample(ScenicSpotExample example);

    int deleteByPrimaryKey(Integer spotId);

    int insert(ScenicSpotWithBLOBs record);

    int insertSelective(ScenicSpotWithBLOBs record);

    List<ScenicSpotWithBLOBs> selectByExampleWithBLOBs(ScenicSpotExample example);

    List<ScenicSpot> selectByExample(ScenicSpotExample example);

    ScenicSpotWithBLOBs selectByPrimaryKey(Integer spotId);

    int updateByExampleSelective(@Param("record") ScenicSpotWithBLOBs record, @Param("example") ScenicSpotExample example);

    int updateByExampleWithBLOBs(@Param("record") ScenicSpotWithBLOBs record, @Param("example") ScenicSpotExample example);

    int updateByExample(@Param("record") ScenicSpot record, @Param("example") ScenicSpotExample example);

    int updateByPrimaryKeySelective(ScenicSpotWithBLOBs record);

    int updateByPrimaryKeyWithBLOBs(ScenicSpotWithBLOBs record);

    int updateByPrimaryKey(ScenicSpot record);

    int findMaxSort();
}