package com.carl.guli.mapper;

import com.carl.guli.model.Feedback;
import com.carl.guli.model.FeedbackExample;
import com.carl.guli.model.FeedbackWithBLOBs;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface FeedbackMapper {
    int countByExample(FeedbackExample example);

    int deleteByExample(FeedbackExample example);

    int deleteByPrimaryKey(Integer fbId);

    int insert(FeedbackWithBLOBs record);

    int insertSelective(FeedbackWithBLOBs record);

    List<FeedbackWithBLOBs> selectByExampleWithBLOBs(FeedbackExample example);

    List<Feedback> selectByExample(FeedbackExample example);

    FeedbackWithBLOBs selectByPrimaryKey(Integer fbId);

    int updateByExampleSelective(@Param("record") FeedbackWithBLOBs record, @Param("example") FeedbackExample example);

    int updateByExampleWithBLOBs(@Param("record") FeedbackWithBLOBs record, @Param("example") FeedbackExample example);

    int updateByExample(@Param("record") Feedback record, @Param("example") FeedbackExample example);

    int updateByPrimaryKeySelective(FeedbackWithBLOBs record);

    int updateByPrimaryKeyWithBLOBs(FeedbackWithBLOBs record);

    int updateByPrimaryKey(Feedback record);
}