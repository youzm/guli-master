package com.carl.guli.common.exception;

/**
 * Created by youzm on 16/9/7.
 */
/**
 * 业务异常，回滚事务
 */
public class BusinessException extends RuntimeException {
    private String errorCode;

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }


    public BusinessException(String message) {
        super(message);
    }

    public BusinessException(String message, Throwable cause) {
        super(message, cause);
    }

    public BusinessException(String errorCode, String message) {
        super(message, null);
        this.errorCode = errorCode;
    }
}
