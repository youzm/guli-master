package com.carl.guli.common.constant;

import java.util.ResourceBundle;

/**
 * Created by youzm on 16/9/8.
 */
public class Constant {

    public static final String CACHE_ACCOUNT = "cache_account";

    public static ResourceBundle rsb = ResourceBundle.getBundle("jdbc");

    public static String getConfigValue(String configKey) {
        return rsb.getString(configKey);
    }

    public static String downloadUrl;

    static {
        downloadUrl = getConfigValue("ftp.download");
    }
}
