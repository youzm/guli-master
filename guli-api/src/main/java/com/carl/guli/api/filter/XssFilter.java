package com.carl.guli.api.filter;

import javax.servlet.*;
import java.io.IOException;

/**
 * Created by youzm on 2017/5/17.
 */
public class XssFilter implements Filter {
    public void destroy() {
    }

    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws ServletException, IOException {
        chain.doFilter(req, resp);
    }

    public void init(FilterConfig config) throws ServletException {

    }

}
