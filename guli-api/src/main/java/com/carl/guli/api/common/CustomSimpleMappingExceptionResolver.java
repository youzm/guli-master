package com.carl.guli.api.common;

import com.alibaba.fastjson.JSON;
import com.carl.guli.dto.Result;
import org.apache.commons.lang.StringUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.SimpleMappingExceptionResolver;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by youzm on 2016/12/28.
 */
public class CustomSimpleMappingExceptionResolver extends
        SimpleMappingExceptionResolver {
    @Override
    protected ModelAndView doResolveException(HttpServletRequest request,
                                              HttpServletResponse response, Object handler, Exception ex) {
        // Expose ModelAndView for chosen error view.
        ex.printStackTrace();
        String viewName = determineViewName(ex, request);

        Result result = new Result(0, StringUtils.isBlank(ex.getMessage()) ? String.valueOf(ex) : ex.getMessage());
        request.setAttribute("result", result);

        if (viewName != null) {// JSP格式返回
            if (!(request.getHeader("accept").indexOf("application/json") > -1 || (request
                    .getHeader("X-Requested-With") != null && request
                    .getHeader("X-Requested-With").indexOf("XMLHttpRequest") > -1))) {
                // 如果不是异步请求
                // Apply HTTP status code for error views, if specified.
                // Only apply it if we're processing a top-level request.
                Integer statusCode = determineStatusCode(request, viewName);
                if (statusCode != null) {
                    applyStatusCodeIfPossible(request, response, statusCode);
                }
                return getModelAndView(viewName, ex, request);
            } else {// JSON格式返回
                try {
                    response.setStatus(org.springframework.http.HttpStatus.OK.value());
                    response.setContentType("application/json");
                    response.setCharacterEncoding("UTF-8");
                    response.setHeader("Cache-Control","no-cache,must-revalidate");
                    PrintWriter writer = response.getWriter();
                    writer.write(JSON.toJSONString(result));
                    writer.close();


//                    PrintWriter writer = response.getWriter();
//                    writer.write(ex.getMessage());
//                    writer.flush();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return null;

            }
        } else {
            return null;
        }
    }
}
