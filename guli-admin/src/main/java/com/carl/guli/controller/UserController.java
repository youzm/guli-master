package com.carl.guli.controller;

import com.carl.guli.dto.Result;
import com.carl.guli.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;

/**
 * Created by youzm on 2017/3/13.
 */
@Controller
@RequestMapping("/user")
public class UserController extends BaseController{

    private static final Logger logger = LoggerFactory.getLogger(UserController.class);


    @Value("${ftp.directory}")
    private String ftp;

    @Autowired
    UserService userService;

    @RequestMapping("/pswd/init.do")
    public void initUserPassword(){
        userService.initUserPassword();
    }

    @RequestMapping("/pswd/modify.do")
    @ResponseBody
    public Result modifyPassword(HttpSession session, String oldPassword, String password){
        userService.modifyPassword(getSessionAccount(session).getAccountId(),oldPassword,password);
        return new Result(1,"");
    }
}
