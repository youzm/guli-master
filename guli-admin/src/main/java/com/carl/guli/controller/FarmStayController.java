package com.carl.guli.controller;

import com.carl.guli.common.exception.BusinessException;
import com.carl.guli.dto.Result;
import com.carl.guli.model.Banner;
import com.carl.guli.model.FarmStay;
import com.carl.guli.model.ScenicSpot;
import com.carl.guli.service.FarmStayService;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * Created by youzm on 2017/5/9.
 */
@Controller
@RequestMapping("/farmStay")
public class FarmStayController extends BaseController {

    @Autowired
    private FarmStayService farmStayService;

    @RequestMapping("/init.do")
    public ModelAndView initFarmStay() {
        return new ModelAndView("farmStay");
    }

    @RequestMapping("/list.do")
    @ResponseBody
    public PageInfo<FarmStay> listFarmStay() {
//        PageHelper.startPage(dto.getPage().getPageNum(),dto.getPage().getPageSize());
//        throw new BusinessException("错误!!!");


        List<FarmStay> list = farmStayService.listFarmStay();
        PageInfo<FarmStay> pageInfo = new PageInfo(list);
        return pageInfo;
    }

    @RequestMapping(value = "/add.do", method = RequestMethod.GET)
    public ModelAndView initAddFarmStay() {
        return new ModelAndView("farmStay_edit");
    }

    @RequestMapping(value = "/add.do", method = RequestMethod.POST)
    public String addFarmStay(HttpServletRequest request, FarmStay farmStay, @RequestParam("farm_pic") CommonsMultipartFile[] farm_pic,
                              @RequestParam("banner_1") CommonsMultipartFile[] banner1,
                              @RequestParam("banner_2") CommonsMultipartFile[] banner2,
                              @RequestParam("banner_3") CommonsMultipartFile[] banner3,
                              @RequestParam("banner_4") CommonsMultipartFile[] banner4,
                              @RequestParam("banner_5") CommonsMultipartFile[] banner5) throws Exception {
        farmStayService.addFarmStay(farmStay, farm_pic, banner1, banner2, banner3,banner4,banner5);
        return "redirect:init.do";
    }

    @RequestMapping(value = "/edit.do", method = RequestMethod.GET)
    public ModelAndView initEditFarmStay(ModelMap mm, Integer farmId) {
        FarmStay farmStay = farmStayService.findFarmStayById(farmId);

        mm.put("farmStay", farmStay);
        return new ModelAndView("farmStay_edit");
    }

    @RequestMapping(value = "/edit.do", method = RequestMethod.POST)
    @ResponseBody
    public Result editFarmStay(FarmStay farmStay, String oper, Integer id) {
        if (oper.equals("edit")) {
            farmStayService.editFarmStay(farmStay);
        }

        if (oper.equals("del")) {
            farmStayService.delFarmStay(id);
        }

        return new Result(1, "");
    }


    /**
     * 广告位配置
     *
     * @return
     */
    @RequestMapping("/banner/init.do")
    public ModelAndView initBanner() {
        return new ModelAndView("banner");
    }

    @RequestMapping("/banner/list.do")
    @ResponseBody
    public PageInfo<Banner> listBanner(Banner banner, HttpSession session) {
//        PageHelper.startPage(dto.getPage().getPageNum(),dto.getPage().getPageSize());
        List<Banner> list = farmStayService.listBanner();
        PageInfo<Banner> pageInfo = new PageInfo(list);
        return pageInfo;
    }

    @RequestMapping(value = "/banner/add.do", method = RequestMethod.GET)
    public ModelAndView initAddBanner() {
        return new ModelAndView("banner_edit");
    }

    @RequestMapping(value = "/banner/add.do", method = RequestMethod.POST)
    public String addBanner(HttpServletRequest request, Banner banner, @RequestParam("banner_pic") CommonsMultipartFile[] banner_pic) throws Exception {
        farmStayService.addBanner(banner, banner_pic);
        return "redirect:init.do";
    }

    @RequestMapping(value = "/banner/edit.do", method = RequestMethod.GET)
    public ModelAndView initEditBanner(ModelMap mm, Integer bannerId) {
        Banner banner = farmStayService.findBannerById(bannerId);

        mm.put("banner", banner);
        return new ModelAndView("banner_edit");
    }

    @RequestMapping(value = "/banner/edit.do", method = RequestMethod.POST)
    @ResponseBody
    public Result editBanner(Banner banner, String oper, Integer id) {
        if (oper.equals("edit")) {
            farmStayService.editBanner(banner);
        }

        if (oper.equals("del")) {
            farmStayService.delBanner(id);
        }

        return new Result(1, "");
    }
}
