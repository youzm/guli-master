package com.carl.guli.controller;

import com.carl.guli.dto.Result;
import com.carl.guli.model.FarmStay;
import com.carl.guli.model.Good;
import com.carl.guli.service.GoodService;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * Created by youzm on 2017/5/9.
 */
@Controller
@RequestMapping("/good")
public class GoodController extends BaseController{

    @Autowired
    private GoodService goodService;

    @RequestMapping("/init.do")
    public ModelAndView initGood(){
        return new ModelAndView("good");
    }

    @RequestMapping("/list.do")
    @ResponseBody
    public PageInfo<Good> listAccount(Good good, HttpSession session) {
//        PageHelper.startPage(dto.getPage().getPageNum(),dto.getPage().getPageSize());
        List<Good> list = goodService.listGood();
        PageInfo<Good> pageInfo = new PageInfo(list);
        return pageInfo;
    }

    @RequestMapping(value = "/add.do",method = RequestMethod.GET)
    public ModelAndView initAddGood(){
        return new ModelAndView("good_edit");
    }

    @RequestMapping(value = "/add.do",method = RequestMethod.POST)
    public String addGood(HttpServletRequest request, Good good, @RequestParam("good_pic") CommonsMultipartFile[] good_pic,
                          @RequestParam("banner_1") CommonsMultipartFile[] banner1,
                          @RequestParam("banner_2") CommonsMultipartFile[] banner2,
                          @RequestParam("banner_3") CommonsMultipartFile[] banner3,
                          @RequestParam("banner_4") CommonsMultipartFile[] banner4,
                          @RequestParam("banner_5") CommonsMultipartFile[] banner5) throws Exception {
        goodService.addGood(good, good_pic, banner1,banner2,banner3,banner4,banner5);
        return "redirect:init.do";
    }

    @RequestMapping(value = "/edit.do", method = RequestMethod.GET)
    public ModelAndView initEditGood(ModelMap mm, Integer goodId) {
        Good good = goodService.findGoodById(goodId);

        mm.put("good",good);
        return new ModelAndView("good_edit");
    }

    @RequestMapping(value = "/edit.do",method = RequestMethod.POST)
    @ResponseBody
    public Result editGood(Good good, String oper, Integer id){
        if (oper.equals("edit")){
            goodService.editGood(good);
        }

        if (oper.equals("del")){
            goodService.delGood(id);
        }

        return new Result(1,"");
    }


}
