package com.carl.guli.controller;

import com.carl.guli.dto.Result;
import com.carl.guli.model.AccountInfo;
import com.carl.guli.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/**
 * Created by youzm on 2017/3/10.
 */
@Controller
@RequestMapping("/")
public class HomeController extends BaseController {

    private static final Logger logger = LoggerFactory.getLogger(HomeController.class);

    @Autowired
    UserService userService;


    @RequestMapping(value="/login.do",method = RequestMethod.GET)
    public ModelAndView initLogin(){
        return new ModelAndView("login");
    }

    @RequestMapping(value="/login.do",method = RequestMethod.POST)
    @ResponseBody
    public Result login(AccountInfo accountInfo, HttpSession session, String remember, HttpServletRequest request, HttpServletResponse response){

        String name = accountInfo.getJobNumber();               //获取用户名
        String password = accountInfo.getPassword() ;      //获取密码

        accountInfo = userService.login(accountInfo);
        setSessionAccount(session,accountInfo);

        //
//        List<OperationUserDto> list = userService.findOperationUser(accountInfo.getAccountId());
//        setSessionOperation(session,list);

        String codeName="";
        try {
            codeName = URLEncoder.encode(name, "UTF-8");      //对输入的中文进行编码，防止乱码出现
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        Cookie nameCookie = new Cookie("name", codeName);
        Cookie passwordCookie = new Cookie("password", password);
        nameCookie.setPath(request.getContextPath()+"/");      //设置Cookie的有效路径
        passwordCookie.setPath(request.getContextPath()+"/");//设置Cookie的有效路径
        if(remember != null && "yes".equals(remember)){            //有记住我，就设置cookie的保存时间
            nameCookie.setMaxAge(7*24*60*60);
            passwordCookie.setMaxAge(7*24*60*60);
        }else{                                                                                 //没有记住我，设置cookie的时间为0
            nameCookie.setMaxAge(0);
            passwordCookie.setMaxAge(0);
        }
        response.addCookie(nameCookie);
        response.addCookie(passwordCookie);

        return new Result(1,"");
    }

    @RequestMapping("/logout.do")
    public String logout(HttpSession session) {
        if (session != null) {
            session.invalidate();
        }
        return "redirect:login.do";
    }

    @RequestMapping(value="/dashboard.do",method = RequestMethod.GET)
    public ModelAndView initDashboard(ModelMap mm){
        return new ModelAndView("dashboard");
    }
}
