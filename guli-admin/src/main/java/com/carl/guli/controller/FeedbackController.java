package com.carl.guli.controller;

import com.carl.guli.dto.FeedbackDto;
import com.carl.guli.dto.Result;
import com.carl.guli.model.*;
import com.carl.guli.service.FarmStayService;
import com.carl.guli.service.FeedbackService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * Created by youzm on 2017/5/9.
 */
@Controller
@RequestMapping("/feedback")
public class FeedbackController extends BaseController{

    @Autowired
    private FeedbackService feedbackService;

    @RequestMapping("/init.do")
    public ModelAndView initFeedback(){
        return new ModelAndView("feedback");
    }

    @RequestMapping("/list.do")
    @ResponseBody
    public PageInfo<FeedbackWithBLOBs> listFarmStay(FeedbackDto dto) {
        PageHelper.startPage(dto.getPage().getPageNum(),dto.getPage().getPageSize());
        List<FeedbackWithBLOBs> list = feedbackService.listFeedback();
        PageInfo<FeedbackWithBLOBs> pageInfo = new PageInfo(list);
        return pageInfo;
    }

    @RequestMapping(value = "/edit.do",method = RequestMethod.POST)
    @ResponseBody
    public Result editFeedback(Feedback feedback, String oper, Integer id){
        if (oper.equals("edit")){
//            feedbackService.editScenicSpot(scenicSpot);
        }

        if (oper.equals("del")){
            feedbackService.delFeedback(id);
        }

        return new Result(1,"");
    }
}
