package com.carl.guli.controller;

import com.carl.guli.dto.Result;
import com.carl.guli.model.ScenicSpot;
import com.carl.guli.model.ScenicSpotWithBLOBs;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * Created by youzm on 2017/5/8.
 */
@Controller
@RequestMapping("/scenicSpot")
public class ScenicSpotController extends BaseController {

    @Autowired
    com.carl.guli.service.ScenicSpotService scenicSpotService;

    @RequestMapping("/init.do")
    public ModelAndView init() {
        return new ModelAndView("scenicSpot");
    }

    @RequestMapping("/list.do")
    @ResponseBody
    public PageInfo<ScenicSpot> listAccount(ScenicSpot scenicSpot, HttpSession session) {
//        PageHelper.startPage(dto.getPage().getPageNum(),dto.getPage().getPageSize());
        List<ScenicSpot> list = scenicSpotService.listScenicSpot();
        PageInfo<ScenicSpot> pageInfo = new PageInfo(list);
        return pageInfo;
    }

    @RequestMapping(value = "/add.do", method = RequestMethod.GET)
    public ModelAndView initAddScenicSpot() {
        return new ModelAndView("scenicSpot_edit");
    }

    @RequestMapping(value = "/add.do", method = RequestMethod.POST)
    public String AddScenicSpot(HttpServletRequest request, ScenicSpotWithBLOBs scenicSpot, @RequestParam("intro_pic") CommonsMultipartFile[] introPic,
                              @RequestParam("back_pic") CommonsMultipartFile[] backPic) throws Exception {
        scenicSpotService.AddScenicSpot(scenicSpot, introPic,backPic);
        return "redirect:init.do";
    }

    @RequestMapping(value = "/edit.do",method = RequestMethod.POST)
    @ResponseBody
    public Result editScenicSpot( ScenicSpotWithBLOBs scenicSpot, String oper, Integer id){
        if (oper.equals("edit")){
            scenicSpotService.editScenicSpot(scenicSpot);
        }

        if (oper.equals("del")){
            scenicSpotService.delScenicSpot(id);
        }

        return new Result(1,"");
    }

    @RequestMapping(value = "/edit.do",method = RequestMethod.GET)
    public ModelAndView initEditScenicSpot(ModelMap mm, Integer spotId) {
        ScenicSpotWithBLOBs scenicSpot = scenicSpotService.findScenicSpotById(spotId);

        mm.put("spot",scenicSpot);
        return new ModelAndView("scenicSpot_edit");
    }

    @RequestMapping(value = "/top.do")
    @ResponseBody
    public Result update2Top(Integer spotId) {
        scenicSpotService.update2Top(spotId);

        return new Result(1,"");
    }
}
