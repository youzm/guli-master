package com.carl.guli.controller;

import com.carl.guli.dto.Result;
import com.carl.guli.model.Banner;
import com.carl.guli.model.FarmStay;
import com.carl.guli.model.Hotel;
import com.carl.guli.service.FarmStayService;
import com.carl.guli.service.HotelService;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * Created by youzm on 2017/5/9.
 */
@Controller
@RequestMapping("/hotel")
public class HotelController extends BaseController{

    @Autowired
    private HotelService hotelService;

    @RequestMapping("/init.do")
    public ModelAndView initHotel(){
        return new ModelAndView("hotel");
    }

    @RequestMapping("/list.do")
    @ResponseBody
    public PageInfo<Hotel> listFarmStay() {
//        PageHelper.startPage(dto.getPage().getPageNum(),dto.getPage().getPageSize());
        List<Hotel> list = hotelService.listHotel();
        PageInfo<Hotel> pageInfo = new PageInfo(list);
        return pageInfo;
    }

    @RequestMapping(value = "/add.do",method = RequestMethod.GET)
    public ModelAndView initAddHotel(){
        return new ModelAndView("hotel_edit");
    }

    @RequestMapping(value = "/add.do",method = RequestMethod.POST)
    public String addHotel(HttpServletRequest request, Hotel hotel, @RequestParam("hotel_pic") CommonsMultipartFile[] hotel_pic,
                            @RequestParam("banner_1") CommonsMultipartFile[] banner1,
                            @RequestParam("banner_2") CommonsMultipartFile[] banner2,
                            @RequestParam("banner_3") CommonsMultipartFile[] banner3) throws Exception {
        hotelService.addHotel(hotel, hotel_pic, banner1,banner2,banner3);
        return "redirect:init.do";
    }

    @RequestMapping(value = "/edit.do",method = RequestMethod.GET)
    public ModelAndView initEditHotel(ModelMap mm, Integer hotelId) {
        Hotel hotel = hotelService.findHotelById(hotelId);

        mm.put("hotel",hotel);
        return new ModelAndView("hotel_edit");
    }

    @RequestMapping(value = "/edit.do",method = RequestMethod.POST)
    @ResponseBody
    public Result editHotel(Hotel hotel, String oper, Integer id){
        if (oper.equals("edit")){
            hotelService.editHotel(hotel);
        }

        if (oper.equals("del")){
            hotelService.delHotel(id);
        }

        return new Result(1,"");
    }
}
