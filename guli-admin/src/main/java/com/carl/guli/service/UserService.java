package com.carl.guli.service;

import com.carl.guli.model.AccountInfo;

/**
 * Created by youzm on 2017/3/10.
 */
public interface UserService {
    AccountInfo login(AccountInfo accountInfo);

    void setPassword(AccountInfo accountInfo);

    void initUserPassword();

    void modifyPassword(Integer accountId, String oldPassword, String password);
}
