package com.carl.guli.service;

import com.carl.guli.model.Good;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import java.util.List;

/**
 * Created by youzm on 2017/5/9.
 */
public interface GoodService {
    void addGood(Good good, CommonsMultipartFile[] goodPic, CommonsMultipartFile[] commonsMultipartFiles, CommonsMultipartFile[] good_pic, CommonsMultipartFile[] banner1, CommonsMultipartFile[] banner2, CommonsMultipartFile[] banner3) throws Exception;

    List<Good> listGood();

    Good findGoodById(Integer goodId);

    void editGood(Good good);

    void delGood(Integer id);
}
