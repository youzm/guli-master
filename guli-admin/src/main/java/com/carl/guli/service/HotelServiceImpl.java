package com.carl.guli.service;

import com.carl.guli.common.util.FileUploadUtil;
import com.carl.guli.mapper.BannerMapper;
import com.carl.guli.mapper.FarmStayMapper;
import com.carl.guli.mapper.HotelMapper;
import com.carl.guli.model.FarmStay;
import com.carl.guli.model.FarmStayExample;
import com.carl.guli.model.Hotel;
import com.carl.guli.model.HotelExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import java.util.List;

/**
 * Created by youzm on 2017/5/11.
 */
@Service
public class HotelServiceImpl implements HotelService{

    @Value("${ftp.directory}")
    private String ftpPath;

    @Value("${ftp.pic}")
    private String ftpPic;

    @Value("${ftp.download}")
    private String ftpDownload;

    @Autowired
    HotelMapper hotelMapper;

    @Override
    public List<Hotel> listHotel() {
        return hotelMapper.selectByExample(null);
    }

    @Override
    public Hotel findHotelById(Integer hotelId) {
        HotelExample example = new HotelExample();
        example.createCriteria().andHotelIdEqualTo(hotelId);
        List<Hotel> hotels = hotelMapper.selectByExampleWithBLOBs(example);
        if (hotels.size() > 0){
            return hotels.get(0);
        }
        return null;
    }

    @Override
    public void addHotel(Hotel hotel, CommonsMultipartFile[] hotel_pic, CommonsMultipartFile[] banner1, CommonsMultipartFile[] banner2, CommonsMultipartFile[] banner3) throws Exception {
        if (!hotel_pic[0].isEmpty()){
            String farmPicStr = FileUploadUtil.uploadImg(ftpPath + ftpPic, hotel_pic[0]);
            hotel.setHotelPic(ftpDownload + ftpPic + "/" + farmPicStr);
        }
        if (!banner1[0].isEmpty()){
            String banner1Str = FileUploadUtil.uploadImg(ftpPath + ftpPic, banner1[0]);
            hotel.setBanner1(ftpDownload + ftpPic + "/" + banner1Str);
        }
        if (!banner2[0].isEmpty()){
            String banner2Str = FileUploadUtil.uploadImg(ftpPath + ftpPic, banner2[0]);
            hotel.setBanner2(ftpDownload + ftpPic + "/" + banner2Str);
        }
        if (!banner3[0].isEmpty()){
            String banner3Str = FileUploadUtil.uploadImg(ftpPath + ftpPic, banner3[0]);
            hotel.setBanner3(ftpDownload + ftpPic + "/" + banner3Str);
        }

        if (hotel.getHotelId() == null){
            hotelMapper.insertSelective(hotel);
        }

        if (hotel.getHotelId() != null){
            hotelMapper.updateByPrimaryKeySelective(hotel);
        }
    }

    @Override
    public void editHotel(Hotel hotel) {
        hotelMapper.updateByPrimaryKeySelective(hotel);
    }

    @Override
    public void delHotel(Integer id) {
        hotelMapper.deleteByPrimaryKey(id);
    }
}
