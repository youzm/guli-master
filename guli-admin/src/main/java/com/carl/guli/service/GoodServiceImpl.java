package com.carl.guli.service;

import com.carl.guli.common.util.FileUploadUtil;
import com.carl.guli.mapper.GoodMapper;
import com.carl.guli.model.Good;
import com.carl.guli.model.GoodExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import java.util.List;

/**
 * Created by youzm on 2017/5/9.
 */
@Service
public class GoodServiceImpl implements GoodService{

    @Value("${ftp.directory}")
    private String ftpPath;

    @Value("${ftp.pic}")
    private String ftpPic;

    @Value("${ftp.download}")
    private String ftpDownload;

    @Autowired
    private GoodMapper goodMapper;

    @Override
    public void addGood(Good good, CommonsMultipartFile[] good_pic, CommonsMultipartFile[] banner1, CommonsMultipartFile[] banner2, CommonsMultipartFile[] banner3,
                        CommonsMultipartFile[] banner4,
                        CommonsMultipartFile[] banner5) throws Exception {
        if (!good_pic[0].isEmpty()){
            String farmPicStr = FileUploadUtil.uploadImg(ftpPath + ftpPic, good_pic[0]);
            good.setGoodPic(ftpDownload + ftpPic + "/" + farmPicStr);
        }
        if (!banner1[0].isEmpty()){
            String banner1Str = FileUploadUtil.uploadImg(ftpPath + ftpPic, banner1[0]);
            good.setBanner1(ftpDownload + ftpPic + "/" + banner1Str);
        }
        if (!banner2[0].isEmpty()){
            String banner2Str = FileUploadUtil.uploadImg(ftpPath + ftpPic, banner2[0]);
            good.setBanner2(ftpDownload + ftpPic + "/" + banner2Str);
        }
        if (!banner3[0].isEmpty()){
            String banner3Str = FileUploadUtil.uploadImg(ftpPath + ftpPic, banner3[0]);
            good.setBanner3(ftpDownload + ftpPic + "/" + banner3Str);
        }
        if (!banner4[0].isEmpty()){
            String banner4Str = FileUploadUtil.uploadImg(ftpPath + ftpPic, banner4[0]);
            good.setBanner4(ftpDownload + ftpPic + "/" + banner4Str);
        }
        if (!banner5[0].isEmpty()){
            String banner5Str = FileUploadUtil.uploadImg(ftpPath + ftpPic, banner5[0]);
            good.setBanner5(ftpDownload + ftpPic + "/" + banner5Str);
        }

        if (good.getGoodId() == null){
            goodMapper.insertSelective(good);
        }

        if (good.getGoodId() != null){
            goodMapper.updateByPrimaryKeySelective(good);
        }


    }

    @Override
    public List<Good> listGood() {
        GoodExample example = new GoodExample();
        example.setOrderByClause("sort desc");
        return goodMapper.selectByExample(example);
    }

    @Override
    public Good findGoodById(Integer goodId) {
        GoodExample example = new GoodExample();
        example.createCriteria().andGoodIdEqualTo(goodId);
        List<Good> goods = goodMapper.selectByExampleWithBLOBs(example);
        if (goods.size() > 0){
            return goods.get(0);
        }
        return null;
    }

    @Override
    public void editGood(Good good) {
        goodMapper.updateByPrimaryKeySelective(good);
    }

    @Override
    public void delGood(Integer id) {
        goodMapper.deleteByPrimaryKey(id);
    }
}
