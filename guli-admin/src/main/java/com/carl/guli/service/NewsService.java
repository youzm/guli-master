package com.carl.guli.service;

import com.carl.guli.model.News;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import java.util.List;

/**
 * Created by youzm on 2017/5/16.
 */
public interface NewsService {
    List<News> listNews();

    void addNews(News news, CommonsMultipartFile[] news_pic) throws Exception;

    
    News findNewsById(Integer newsId);

    
    void editNews(News news);

    
    void delNews(Integer id);

}
