package com.carl.guli.service;

import com.carl.guli.common.util.FileUploadUtil;
import com.carl.guli.mapper.BannerMapper;
import com.carl.guli.mapper.FarmStayMapper;
import com.carl.guli.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import java.util.List;

/**
 * Created by youzm on 2017/5/9.
 */
@Service
public class FarmStayServiceImpl implements FarmStayService{

    @Value("${ftp.directory}")
    private String ftpPath;

    @Value("${ftp.pic}")
    private String ftpPic;

    @Value("${ftp.download}")
    private String ftpDownload;

    @Autowired
    private FarmStayMapper farmStayMapper;

    @Autowired
    private BannerMapper bannerMapper;

    @Override
    public void addFarmStay(FarmStay farmStay, CommonsMultipartFile[] farm_pic, CommonsMultipartFile[] banner1, CommonsMultipartFile[] banner2, CommonsMultipartFile[] banner3, CommonsMultipartFile[] banner4, CommonsMultipartFile[] banner5) throws Exception {
        if (!farm_pic[0].isEmpty()){
            String farmPicStr = FileUploadUtil.uploadImg(ftpPath + ftpPic, farm_pic[0]);
            farmStay.setFarmPic(ftpDownload + ftpPic + "/" + farmPicStr);
        }
        if (!banner1[0].isEmpty()){
            String banner1Str = FileUploadUtil.uploadImg(ftpPath + ftpPic, banner1[0]);
            farmStay.setBanner1(ftpDownload + ftpPic + "/" + banner1Str);
        }
        if (!banner2[0].isEmpty()){
            String banner2Str = FileUploadUtil.uploadImg(ftpPath + ftpPic, banner2[0]);
            farmStay.setBanner2(ftpDownload + ftpPic + "/" + banner2Str);
        }
        if (!banner3[0].isEmpty()){
            String banner3Str = FileUploadUtil.uploadImg(ftpPath + ftpPic, banner3[0]);
            farmStay.setBanner3(ftpDownload + ftpPic + "/" + banner3Str);
        }
        if (!banner4[0].isEmpty()){
            String banner4Str = FileUploadUtil.uploadImg(ftpPath + ftpPic, banner4[0]);
            farmStay.setBanner4(ftpDownload + ftpPic + "/" + banner4Str);
        }
        if (!banner5[0].isEmpty()){
            String banner5Str = FileUploadUtil.uploadImg(ftpPath + ftpPic, banner5[0]);
            farmStay.setBanner5(ftpDownload + ftpPic + "/" + banner5Str);
        }

        if (farmStay.getFarmId() == null){
            farmStayMapper.insertSelective(farmStay);
        }

        if (farmStay.getFarmId() != null){
            farmStayMapper.updateByPrimaryKeySelective(farmStay);
        }
    }

    @Override
    public List<FarmStay> listFarmStay() {
        return farmStayMapper.selectByExample(null);
    }

    @Override
    public FarmStay findFarmStayById(Integer farmId) {
        FarmStayExample example = new FarmStayExample();
        example.createCriteria().andFarmIdEqualTo(farmId);
        List<FarmStay> farmStays = farmStayMapper.selectByExampleWithBLOBs(example);
        if (farmStays.size() > 0){
            return farmStays.get(0);
        }
        return null;
    }

    @Override
    public List<Banner> listBanner() {
        BannerExample example = new BannerExample();
        example.setOrderByClause("sort desc");
        return bannerMapper.selectByExample(example);
    }

    @Override
    public void addBanner(Banner banner, CommonsMultipartFile[] banner_pic) throws Exception {
        if (!banner_pic[0].isEmpty()){
            String farmPicStr = FileUploadUtil.uploadImg(ftpPath + ftpPic, banner_pic[0]);
            banner.setPic(ftpDownload + ftpPic + "/" + farmPicStr);
        }

        if (banner.getBannerId() == null){
            bannerMapper.insertSelective(banner);
        }

        if (banner.getBannerId() != null){
            bannerMapper.updateByPrimaryKeySelective(banner);
        }
    }

    @Override
    public Banner findBannerById(Integer bannerId) {
        return bannerMapper.selectByPrimaryKey(bannerId);
    }

    @Override
    public void editFarmStay(FarmStay farmStay) {
        farmStayMapper.updateByPrimaryKeySelective(farmStay);
    }

    @Override
    public void delFarmStay(Integer id) {
        farmStayMapper.deleteByPrimaryKey(id);
    }

    @Override
    public void editBanner(Banner banner) {
        bannerMapper.updateByPrimaryKeySelective(banner);
    }

    @Override
    public void delBanner(Integer id) {
        bannerMapper.deleteByPrimaryKey(id);
    }
}
