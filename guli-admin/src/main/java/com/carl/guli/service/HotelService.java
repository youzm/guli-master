package com.carl.guli.service;

import com.carl.guli.model.FarmStay;
import com.carl.guli.model.Hotel;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import java.util.List;

/**
 * Created by youzm on 2017/5/11.
 */
public interface HotelService {
    List<Hotel> listHotel();

    Hotel findHotelById(Integer farmId);

    void addHotel(Hotel hotel, CommonsMultipartFile[] hotel_pic, CommonsMultipartFile[] banner1, CommonsMultipartFile[] banner2, CommonsMultipartFile[] banner3) throws Exception;

    void editHotel(Hotel hotel);

    void delHotel(Integer id);
}
