package com.carl.guli.service;

import com.carl.guli.mapper.FeedbackMapper;
import com.carl.guli.model.FeedbackExample;
import com.carl.guli.model.FeedbackWithBLOBs;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by youzm on 2017/5/11.
 */
@Service
public class FeedbackServiceImpl implements FeedbackService{

    @Autowired
    private FeedbackMapper feedbackMapper;

    @Override
    public List<FeedbackWithBLOBs> listFeedback() {
        FeedbackExample example = new FeedbackExample();
        example.setOrderByClause("create_time desc");
        return feedbackMapper.selectByExampleWithBLOBs(example);
    }

    @Override
    public void delFeedback(Integer id) {
        feedbackMapper.deleteByPrimaryKey(id);
    }
}
