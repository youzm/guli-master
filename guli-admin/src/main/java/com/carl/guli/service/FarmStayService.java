package com.carl.guli.service;

import com.carl.guli.model.Banner;
import com.carl.guli.model.FarmStay;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import java.util.List;

/**
 * Created by youzm on 2017/5/9.
 */
public interface FarmStayService {
    void addFarmStay(FarmStay farmStay, CommonsMultipartFile[] farm_pic, CommonsMultipartFile[] banner1, CommonsMultipartFile[] banner2, CommonsMultipartFile[] banner3, CommonsMultipartFile[] banner4, CommonsMultipartFile[] banner5) throws Exception;

    List<FarmStay> listFarmStay();

    FarmStay findFarmStayById(Integer farmId);

    List<Banner> listBanner();

    void addBanner(Banner banner, CommonsMultipartFile[] banner_pic) throws Exception;

    Banner findBannerById(Integer bannerId);

    void editFarmStay(FarmStay farmStay);

    void delFarmStay(Integer id);

    void editBanner(Banner banner);

    void delBanner(Integer id);
}
