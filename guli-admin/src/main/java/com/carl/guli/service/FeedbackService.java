package com.carl.guli.service;

import com.carl.guli.model.FeedbackWithBLOBs;

import java.util.List;

/**
 * Created by youzm on 2017/5/11.
 */
public interface FeedbackService {
    List<FeedbackWithBLOBs> listFeedback();

    void delFeedback(Integer id);
}
