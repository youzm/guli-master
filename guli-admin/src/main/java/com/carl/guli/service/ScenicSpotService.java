package com.carl.guli.service;

import com.carl.guli.model.ScenicSpot;
import com.carl.guli.model.ScenicSpotWithBLOBs;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import java.util.List;

/**
 * Created by youzm on 2017/5/8.
 */
public interface ScenicSpotService {
    void AddScenicSpot(ScenicSpotWithBLOBs scenicSpot, CommonsMultipartFile[] introPic, CommonsMultipartFile[] backPic) throws Exception;

    List<ScenicSpot> listScenicSpot();

    ScenicSpotWithBLOBs findScenicSpotById(Integer spotId);

    void update2Top(Integer spotId);

    void editScenicSpot(ScenicSpotWithBLOBs scenicSpot);

    void delScenicSpot(Integer id);
}
