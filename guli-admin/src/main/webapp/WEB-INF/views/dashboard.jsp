<%@ page language="java" import="java.util.*" pageEncoding="UTF-8" %>
<%@ include file="./header.jsp" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <meta charset="utf-8"/>
    <title>首页 - 谷里后台系统</title>

    <meta name="description" content="overview &amp; stats"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0"/>

    <!-- bootstrap & fontawesome -->
    <link rel="stylesheet" href="${ctx}/static/assets/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="${ctx}/static/assets/font-awesome/4.5.0/css/font-awesome.min.css"/>
    <link rel="stylesheet" href="${ctx}/static/assets/font-awesome/3.0/css/font-awesome.min.css"/>
    <!-- page specific plugin styles -->

    <!-- text fonts -->
    <link rel="stylesheet" href="${ctx}/static/assets/css/fonts.googleapis.com.css"/>

    <!-- ace styles -->
    <link rel="stylesheet" href="${ctx}/static/assets/css/ace.min.css" class="ace-main-stylesheet" id="main-ace-style"/>

    <!--[if lte IE 9]>
    <link rel="stylesheet" href="${ctx}/static/assets/css/ace-part2.min.css" class="ace-main-stylesheet"/>
    <![endif]-->
    <link rel="stylesheet" href="${ctx}/static/assets/css/ace-skins.min.css"/>
    <link rel="stylesheet" href="${ctx}/static/assets/css/ace-rtl.min.css"/>

    <!--[if lte IE 9]>
    <link rel="stylesheet" href="${ctx}/static/assets/css/ace-ie.min.css"/>
    <![endif]-->
    <!-- Ai私有样式 -->
    <link rel="stylesheet" href="${ctx}/static/css/ai-main.css"/>
    <!-- inline styles related to this page -->
    <!-- ace settings handler -->
    <script src="${ctx}/static/assets/js/ace-extra.min.js"></script>

    <!-- HTML5shiv and Respond.js for IE8 to support HTML5 elements and media queries -->

    <!--[if lte IE 8]>
    <script src="${ctx}/static/assets/js/html5shiv.min.js"></script>
    <script src="${ctx}/static/assets/js/respond.min.js"></script>
    <![endif]-->
    <link rel="stylesheet" type="text/css" href="${ctx}/static/layer/skin/default/layer.css"/>
    <style>
    .page.unitBox {
        background:#fff url(${ctx}/static/assets/images/main_bg.png) no-repeat center center;
    }
    .navTop {
        text-align:center;
        height:41px;
        line-height:41px;
        font-weight:bold;
    }
    </style>

</head>

<body class="no-skin skin-3">
<div id="navbar" class="navbar navbar-default          ace-save-state">
    <div class="navbar-container ace-save-state" id="navbar-container">
        <%--<button type="button" class="navbar-toggle menu-toggler pull-left" id="menu-toggler" data-target="#sidebar">--%>
            <%--<span class="sr-only">Toggle sidebar</span>--%>

            <%--<span class="icon-bar"></span>--%>

            <%--<span class="icon-bar"></span>--%>

            <%--<span class="icon-bar"></span>--%>
        <%--</button>--%>

        <div class="navbar-header pull-left">
            <a href="${ctx}/dashboard.do" class="navbar-brand">
                <small>
                    <%--<i class="fa fa-leaf"></i>--%>
                    <%--<img src="${ctx}/static/assets/images/logo1.png" width="50px">--%>
                        谷里微信后台管理系统
                </small>
            </a>
        </div>

        <div class="navbar-buttons navbar-header pull-right" role="navigation">
            <ul class="nav ace-nav">
                <li class="light-blue dropdown-modal" id="setting-modal">
                    <a data-toggle="dropdown" href="#" class="dropdown-toggle">
                        <%--<img class="nav-user-photo" src="${ctx}/static/assets/images/avatars/user.jpg"--%>
                        <%--alt="Jason's Photo"/>--%>
								<span class="user-info">
									<small>欢迎,</small>
									${sessionScope.cache_account.name}
								</span>

                        <i class="ace-icon fa fa-caret-down"></i>
                    </a>

                    <ul class="user-menu dropdown-menu-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">
                        <li>
                            <a onclick="hide_drop_menu()">
                                <i class="ace-icon fa fa-cog"></i>
                                修改密码
                            </a>
                        </li>
                        <li>
                            <a href="${ctx}/logout.do">
                                <i class="ace-icon fa fa-power-off"></i>
                                退出
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div><!-- /.navbar-container -->
</div>

<div class="main-container ace-save-state" id="main-container">
    <script type="text/javascript">
        try {
            ace.settings.loadState('main-container')
        } catch (e) {
        }
    </script>

    <div class="main-container-inner">
        <%--<a class="menu-toggler" id="menu-toggler" href="#"> <span class="menu-text"></span> </a>--%>
        <!-- 左侧菜单 -->
        <div class="sidebar" id="sidebar">
            <script type="text/javascript">
                try {
                    ace.settings.check('sidebar', 'fixed')
                } catch (e) {
                }
            </script>
            <div class="navTop">
                微信管理菜单
            </div>
            <ul class="nav nav-list">
                    <li class="">
                        <a href="#" class="dropdown-toggle">
                            <i class="menu-icon fa fa-gift"></i>
							<span class="menu-text">
								特色谷里内容管理
							</span>

                            <b class="arrow fa fa-angle-down"></b>
                        </a>
                        <b class="arrow"></b>
                        <ul class="submenu">
                            <li class="">
                                <a href="${ctx}/scenicSpot/init.do" target="navTab" rel="scenicSpot"
                                   external="true" id="initApply">
                                    <i class="menu-icon fa fa-caret-right"></i>
                                    景点介绍
                                </a>
                                <b class="arrow"></b>
                            </li>
                            <li class="">
                                <a href="${ctx}/farmStay/init.do" target="navTab" rel="farmStay"
                                   external="true">
                                    <i class="menu-icon fa fa-caret-right"></i>
                                    农家乐
                                </a>
                                <b class="arrow"></b>
                            </li>
                            <li class="">
                                <a href="${ctx}/good/init.do" target="navTab" rel="good"
                                   external="true">
                                    <i class="menu-icon fa fa-caret-right"></i>
                                    当地特产
                                </a>
                                <b class="arrow"></b>
                            </li>
                            <li class="">
                                <a href="${ctx}/farmStay/banner/init.do" target="navTab" rel="banner"
                                   external="true">
                                    <i class="menu-icon fa fa-caret-right"></i>
                                    广告位
                                </a>
                                <b class="arrow"></b>
                            </li>
                        </ul>
                    </li>
                <li class="">
                    <a href="#" class="dropdown-toggle">
                        <i class="menu-icon fa fa-leaf"></i>
							<span class="menu-text">
								景点服务内容管理
							</span>

                        <b class="arrow fa fa-angle-down"></b>
                    </a>
                    <b class="arrow"></b>
                    <ul class="submenu">
                        <li class="">
                            <a href="${ctx}/hotel/init.do" target="navTab" rel="hotel"
                               external="true">
                                <i class="menu-icon fa fa-caret-right"></i>
                                酒店
                            </a>
                            <b class="arrow"></b>
                        </li>
                    </ul>
                </li>
                <li class="">
                    <a href="#" class="dropdown-toggle">
                        <i class="menu-icon fa fa-comments"></i>
							<span class="menu-text">
								微互动内容管理
							</span>

                        <b class="arrow fa fa-angle-down"></b>
                    </a>
                    <b class="arrow"></b>
                    <ul class="submenu">
                        <li class="">
                            <a href="${ctx}/news/init.do" target="navTab" rel="news"
                               external="true">
                                <i class="menu-icon fa fa-caret-right"></i>
                                新闻资讯
                            </a>
                            <b class="arrow"></b>
                        </li>
                        <li class="">
                            <a href="${ctx}/feedback/init.do" target="navTab" rel="feedback"
                               external="true">
                                <i class="menu-icon fa fa-caret-right"></i>
                                建议反馈
                            </a>
                            <b class="arrow"></b>
                        </li>
                    </ul>
                </li>

            </ul><!-- /.nav-list -->
            <div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
                <i id="sidebar-toggle-icon" class="ace-icon fa fa-angle-double-left ace-save-state" data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
            </div>
            <script type="text/javascript">
                try {
                    ace.settings.check('sidebar', 'collapsed')
                } catch (e) {
                }
            </script>
        </div>
        <!-- 左侧菜单结束 -->
        <div class="main-content">
            <%--<div class="ace-settings-container" id="ace-settings-container">--%>
                <%--<div class="btn btn-app btn-xs btn-warning ace-settings-btn" id="ace-settings-btn">--%>
                    <%--<i class="ace-icon fa fa-cog bigger-130"></i>--%>
                <%--</div>--%>

                <!-- <div class="ace-settings-box clearfix" id="ace-settings-box"> -->
                    <!-- <div class="pull-left width-50">
                        <div class="ace-settings-item">
                            <div class="pull-left">
                                <select id="skin-colorpicker" class="hide">
                                    <option data-skin="no-skin" value="#53ad59">#53ad59</option>
                                    <option data-skin="skin-1" value="#222A2D">#222A2D</option>
                                    <option data-skin="skin-2" value="#C6487E">#C6487E</option>
                                    <option data-skin="skin-3" value="#D0D0D0">#D0D0D0</option>
                                </select>
                            </div>
                            <span>&nbsp; Choose Skin</span>
                        </div>

                        <div class="ace-settings-item">
                            <input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-navbar" autocomplete="off" />
                            <label class="lbl" for="ace-settings-navbar"> Fixed Navbar</label>
                        </div>

                        <div class="ace-settings-item">
                            <input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-sidebar" autocomplete="off" />
                            <label class="lbl" for="ace-settings-sidebar"> Fixed Sidebar</label>
                        </div>

                        <div class="ace-settings-item">
                            <input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-breadcrumbs" autocomplete="off" />
                            <label class="lbl" for="ace-settings-breadcrumbs"> Fixed Breadcrumbs</label>
                        </div>

                        <div class="ace-settings-item">
                            <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-rtl" autocomplete="off" />
                            <label class="lbl" for="ace-settings-rtl"> Right To Left (rtl)</label>
                        </div>

                        <div class="ace-settings-item">
                            <input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-add-container" autocomplete="off" />
                            <label class="lbl" for="ace-settings-add-container">
                                Inside
                                <b>.container</b>
                            </label>
                        </div>
                    </div> --><!-- /.pull-left -->

                    <!-- <div class="pull-left width-50">
                        <div class="ace-settings-item">
                            <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-hover" autocomplete="off" />
                            <label class="lbl" for="ace-settings-hover"> Submenu on Hover</label>
                        </div>

                        <div class="ace-settings-item">
                            <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-compact" autocomplete="off" />
                            <label class="lbl" for="ace-settings-compact"> Compact Sidebar</label>
                        </div>

                        <div class="ace-settings-item">
                            <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-highlight" autocomplete="off" />
                            <label class="lbl" for="ace-settings-highlight"> Alt. Active Item</label>
                        </div>
                    </div> --><!-- /.pull-left -->
                <!-- </div> --><!-- /.ace-settings-box -->
            <%--</div><!-- /.ace-settings-container -->--%>

            <div id="navTab" class="tabsPage">
                <div class="tabsPageHeader">
                    <div class="tabsPageHeaderContent">
                        <!-- 显示左右控制时添加 class="tabsPageHeaderMargin" -->
                        <ul class="navTab-tab">
                            <li tabid="main" class="main"><a href="javascript:"><span><span
                                    class="home_icon">我的主页</span> </span>
                            </a>
                            </li>
                        </ul>
                    </div>
                    <div class="tabsLeft">left</div>
                    <!-- 禁用只需要添加一个样式 class="tabsLeft tabsLeftDisabled" -->
                    <div class="tabsRight">right</div>
                    <!-- 禁用只需要添加一个样式 class="tabsRight tabsRightDisabled" -->
                    <div class="tabsMore">more</div>
                </div>
                <ul class="tabsMoreList">
                    <li><a href="javascript:">我的主页</a>
                    </li>
                </ul>
                <div class="navTab-panel tabsPageContent layoutBox" style="overflow: auto">
                    <%--<c:if test="${'5' eq sessionScope.provider.userType}">--%>
                    <div class="page unitBox">

                        <i class="icon"></i>
                    </div>
                    <%--</c:if>--%>

                    <%--<c:if test="${'2' eq sessionScope.admin.userType}">
                        <jsp:include page="orderChart.jsp"></jsp:include>
                    </c:if>--%>
                </div>
            </div>

        </div><!-- /.main-content -->
    </div>
</div><!-- /.main-container -->

<div class="modal fade" tabindex="-1" role="dialog" id="codeModal">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">密码修改</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" style="margin: 10px">
                    <div class="form-group">
                        <label class="control-label">原始密码:</label>
                        <input class="form-control" type="password" id="old_password"/>
                    </div>
                    <div class="form-group">
                        <label class="control-label">新密码:</label>
                        <input class="form-control" type="password" id="password"/>
                    </div>
                    <div class="form-group">
                        <label class="control-label">确认新密码:</label>
                        <input class="form-control" type="password" id="re_password"/>
                    </div>
                </form>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                <button type="button" class="btn btn-primary" onclick="modifyPswd()">保存</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<a id="a_transit2" target="navTab" rel="" title="" external="true" style="display: none"></a>

<!-- basic scripts -->

<!--[if !IE]> -->
<script src="${ctx}/static/assets/js/jquery-2.1.4.min.js"></script>

<!-- <![endif]-->

<!--[if IE]>
<script src="${ctx}/static/assets/js/jquery-1.11.3.min.js"></script>
<![endif]-->
<script type="text/javascript">
    if ('ontouchstart' in document.documentElement) document.write("<script src='assets/js/jquery.mobile.custom.min.js'>" + "<" + "/script>");
</script>
<script src="${ctx}/static/assets/js/bootstrap.min.js"></script>

<!-- page specific plugin scripts -->

<!--[if lte IE 8]>
<script src="${ctx}/static/assets/js/excanvas.min.js"></script>
<![endif]-->
<script src="${ctx}/static/assets/js/jquery-ui.custom.min.js"></script>
<script src="${ctx}/static/assets/js/jquery.ui.touch-punch.min.js"></script>
<script src="${ctx}/static/assets/js/jquery.easypiechart.min.js"></script>
<script src="${ctx}/static/assets/js/jquery.sparkline.index.min.js"></script>
<script src="${ctx}/static/assets/js/jquery.flot.min.js"></script>
<script src="${ctx}/static/assets/js/jquery.flot.pie.min.js"></script>
<script src="${ctx}/static/assets/js/jquery.flot.resize.min.js"></script>

<!-- ace scripts -->
<script src="${ctx}/static/assets/js/ace-elements.min.js"></script>
<script src="${ctx}/static/assets/js/ace.min.js"></script>
<!-- kuangjia-->
<script src="${ctx}/static/assets/js/ai.core.js"></script>
<script src="${ctx}/static/layer/layer.js"></script>

<!-- inline scripts related to this page -->
<script type="text/javascript">
    function aClieck2(value, value2) {
        $("#a_transit2").attr("href", value);
        $("#a_transit2").attr("rel", value2);
        $("#a_transit2").attr("title", value2);
        $("#a_transit2").click();
    }

    function hide_drop_menu() {
        $("#setting-modal").removeClass("open");
        showModal();
    }

    function showModal() {
        $('#codeModal').modal('show');
    }

    function modifyPswd() {
        var oldP = $("#old_password").val();
        if(!oldP){
            layer.alert("原始密码不能为空",{icon:2});
            return false;
        }
        var password = $("#password").val();
        if(!password){
            layer.alert("新密码不能为空",{icon:2});
            return false;
        }
        var rPassword = $("#re_password").val();
        if(!rPassword){
            layer.alert("确认信密码不能为空",{icon:2});
            return false;
        }

        if (password != rPassword){
            layer.alert("2次输入的新密码不相同",{icon:2});
            return false;
        }

        $.post("${ctx}/user/pswd/modify.do",{oldPassword:oldP,password:password},function (resp) {
            if (resp.resultCode == 1){
                layer.alert("修改成功",{icon:1});
                $('#codeModal').modal('hide');
            }
            else{
                layer.alert(resp.resultMsg,{icon:2});
            }
        })

    }


    jQuery(function ($) {
//        document.getElementById("initApply").click();
//        $("#skin-colorpicker").click(function(){
//            alert("Hello World  click");
//        });
//        document.getElementById("skin-colorpicker").click();
//        document.getElementById("skin-colorpicker").val("#D0D0D0");
//        $("#skin-colorpicker").val("#D0D0D0");
//        document.getElementById("skin-colorpicker").onchange();
    });


</script>
</body>
</html>
