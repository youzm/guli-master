<%@ page language="java" import="java.util.*" pageEncoding="UTF-8" %>
<%@ include file="./header.jsp" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <meta charset="utf-8"/>
    <title>新闻资讯 - 谷里后台系统</title>

    <meta name="description" content="overview &amp; stats"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0"/>

    <!-- bootstrap & fontawesome -->
    <link rel="stylesheet" href="${ctx}/static/assets/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="${ctx}/static/assets/font-awesome/4.5.0/css/font-awesome.min.css"/>
    <link rel="stylesheet" href="${ctx}/static/assets/font-awesome/3.0/css/font-awesome.min.css"/>
    <link rel="stylesheet" href="${ctx}/static/assets/css/jquery-ui.min.css" />
    <link rel="stylesheet" href="${ctx}/static/assets/css/bootstrap-duallistbox.min.css" />
    <link rel="stylesheet" href="${ctx}/static/assets/css/bootstrap-datepicker3.min.css" />
    <link rel="stylesheet" href="${ctx}/static/assets/css/ui.jqgrid.min.css" />
    <!-- text fonts -->
    <link rel="stylesheet" href="${ctx}/static/assets/css/fonts.googleapis.com.css"/>

    <!-- ace styles -->
    <link rel="stylesheet" href="${ctx}/static/assets/css/ace.min.css"/>
    <!--[if IE 9]>
    <link rel="stylesheet" href="${ctx}/static/assets/css/ace-part2.min.css"/>
    <![endif]-->
    <link rel="stylesheet" href="${ctx}/static/assets/css/ace-rtl.min.css"/>

    <!--[if IE 9]>
    <link rel="stylesheet" href="${ctx}/static/assets/css/ace-ie.min.css"/>
    <![endif]-->

    <!-- HTML5shiv and Respond.js for IE8 to support HTML5 elements and media queries -->

    <!--[if IE 8]>
    <script src="${ctx}/static/assets/js/html5shiv.min.js"></script>
    <script src="${ctx}/static/assets/js/respond.min.js"></script>
    <![endif]-->


    <!--[if !IE]> -->
    <script src="${ctx}/static/assets/js/jquery-2.1.4.min.js"></script>
    <!-- <![endif]-->
    <!--[if IE]>
    <script src="${ctx}/static/assets/js/jquery-1.11.3.min.js"></script>
    <![endif]-->
    <script src="${ctx}/static/assets/js/bootstrap.min.js"></script>
    <script src="${ctx}/static/assets/js/ace-elements.min.js"></script>
    <script src="${ctx}/static/assets/js/ace.min.js"></script>
    <script src="${ctx}/static/assets/js/jquery.bootstrap-duallistbox.min.js"></script>
    <script src="${ctx}/static/assets/js/bootstrap-datepicker.min.js"></script>
    <script src="${ctx}/static/assets/js/jquery.jqGrid.min.js"></script>
    <script src="${ctx}/static/assets/js/grid.locale-en.js"></script>

    <link rel="stylesheet" type="text/css" href="${ctx}/static/layer/skin/default/layer.css"/>
    <script src="${ctx}/static/layer/layer.js"></script>
    <style>
        .ui-jqdialog {
            margin-left: 30%;
        }
        #id{
            display:block;
        }
        .ui-th-column {
            text-align: center !important;
        }
    </style>
</head>

<body class="no-skin">


<div class="page-content">

    <div class="header align-right">
        <a href="${ctx}/news/add.do" class="btn btn-primary">添加新闻资讯</a>
    </div>


    <div class="row">
        <div class="col-xs-12">
            <!-- PAGE CONTENT BEGINS -->

            <table id="grid-table"></table>

            <div id="grid-pager"></div>
            <div class="hr hr-18 dotted hr-double"></div>
            <!-- PAGE CONTENT ENDS -->
        </div><!-- /.col -->
    </div><!-- /.row -->
</div><!-- /.page-content -->

<script type="text/javascript">
    jQuery(function($) {
        var grid_selector = "#grid-table";
        var pager_selector = "#grid-pager";

        var parent_column = $(grid_selector).closest('[class*="col-"]');
        //resize to fit page size
        $(window).on('resize.jqGrid', function () {
            $(grid_selector).jqGrid( 'setGridWidth', parent_column.width() );
        })

        //resize on sidebar collapse/expand
        $(document).on('settings.ace.jqGrid' , function(ev, event_name, collapsed) {
            if( event_name === 'sidebar_collapsed' || event_name === 'main_container_fixed' ) {
                //setTimeout is for webkit only to give time for DOM changes and then redraw!!!
                setTimeout(function() {
                    $(grid_selector).jqGrid( 'setGridWidth', parent_column.width() );
                }, 20);
            }
        })

        jQuery(grid_selector).jqGrid({
            url:"${ctx}/news/list.do",
            datatype:"json",
            mtype:"POST",
            height:"auto",
            autowidth:true,
            colNames:['ID','图片','标题','简介','发布时间','创建时间','操作','详情'],
            colModel:[
                {name:'newsId',index:'news_id', editable: false, key:true,align: 'center',hidden:true},
                {name:'pic',index:'pic', editable:false,align: 'center',
                    formatter: function (cellvalue, options, rowObject) {
                        return "<img height='100px' src=' "+ rowObject.pic +"'>";
                    }},
                {name:'title',index:'title',  editable: true,align: 'center'},
                {name:'intro',index:'intro',editable: true,align: 'center'},
                {name:'publishTime',index:'publishTime',editable: true,align: 'center',unformat: pickDate},
                {name:'createTime',index:'createTime',editable: false,align: 'center'},
                {name:'myac',index:'',  fixed:true, sortable:false, resize:false,
                    formatter:'actions',
                    formatoptions:{
                        keys:true,
                        //delbutton: false,//disable delete button
                        delOptions:{recreateForm: true, beforeShowForm:beforeDeleteCallback},
//                        editformbutton:true, editOptions:{recreateForm: true, beforeShowForm:beforeEditCallback}
                    }
                },
                {
                    name: 'myac2', index: '', sortable: false, resize: false,align: 'center',
                    formatter: function (cellvalue, options, rowObject) {
                        return "<div class='btn-group'><a class='btn btn-xs btn-primary' style='font-weight: bold' onclick='editHotel(" + rowObject.newsId + ")'>详情</a></div>";
                    }
                }

            ],
            sortable:true,
            viewrecords : true,
            rownumbers:true,//添加左侧行号
            rowNum:15,
            rowList:[15,20,30],
            pager : pager_selector,
            altRows: true,
            //toppager: true,

//            multiselect: true,
//            //multikey: "ctrlKey",
//            multiboxonly: true,

            loadComplete : function() {
                var table = this;
                setTimeout(function(){
                    styleCheckbox(table);
                    updateActionIcons(table);
                    updatePagerIcons(table);
                    enableTooltips(table);
                }, 0);
            },

            editurl: "${ctx}/news/edit.do",//nothing is saved
//            caption: "<span style='font-weight: bold'>酒店列表</span>",
            jsonReader: {
                root: "list",
                records:"total",
                total:"pages"
            },
            prmNames:{page:"page.pageNum",rows:"page.pageSize"}
        });

        jQuery(grid_selector).setGridParam().hideCol("ID").trigger("reloadGrid");


        $(window).triggerHandler('resize.jqGrid');//trigger window resize to make the grid get the correct size

        function pickDate( cellvalue, options, cell ) {
            setTimeout(function(){
                $(cell) .find('input[type=text]')
                        .datepicker({format:'yyyy-mm-dd' , autoclose:true});
            }, 0);
        }


        //enable search/filter toolbar
//        jQuery(grid_selector).jqGrid('filterToolbar',{defaultSearch:true,stringResult:true})
//        jQuery(grid_selector).filterToolbar({});


        //switch element when editing inline
        function aceSwitch( cellvalue, options, cell ) {
            setTimeout(function(){
                $(cell) .find('input[type=checkbox]')
                        .addClass('ace ace-switch ace-switch-5')
                        .after('<span class="lbl"></span>');
            }, 0);
        }
        //enable datepicker
        function pickDate( cellvalue, options, cell ) {
            setTimeout(function(){
                $(cell) .find('input[type=text]')
                        .datepicker({format:'yyyy-mm-dd' , autoclose:true});
            }, 0);
        }


        //navButtons
//        jQuery(grid_selector).jqGrid('navGrid',pager_selector,
//                { 	//navbar options
//                    edit: true,
//                    editicon : 'ace-icon fa fa-pencil blue',
//                    add: true,
//                    addicon : 'ace-icon fa fa-plus-circle purple',
//                    del: false,
//                    delicon : 'ace-icon fa fa-trash-o red',
//                    search: false,
////                    searchicon : 'ace-icon fa fa-search orange',
//                    refresh: true,
//                    refreshicon : 'ace-icon fa fa-refresh green',
//                    view: true,
//                    viewicon : 'ace-icon fa fa-search-plus grey',
//                },
//                {
//                    //edit record form
//                    //closeAfterEdit: true,
//                    //width: 700,
//                    recreateForm: true,
//                    beforeShowForm : function(e) {
//                        var form = $(e[0]);
//                        form.closest('.ui-jqdialog').find('.ui-jqdialog-titlebar').wrapInner('<div class="widget-header" />')
//                        style_edit_form(form);
//                    },
//                    afterShowForm : function(e) {
//                        $("#tr_jobNumber").show();
//                    }
//                },
//                {
//                    //new record form
//                    //width: 700,
//                    closeAfterAdd: true,
//                    recreateForm: true,
//                    viewPagerButtons: false,
//                    beforeShowForm : function(e) {
//                        var form = $(e[0]);
//                        form.closest('.ui-jqdialog').find('.ui-jqdialog-titlebar')
//                                .wrapInner('<div class="widget-header" />')
//                        style_edit_form(form);
//                    },
//                    afterShowForm : function(e) {
//                        $("#tr_jobNumber").show();
//                    }
//                },
//                {
//                    //delete record form
//                    recreateForm: true,
//                    beforeShowForm : function(e) {
//                        var form = $(e[0]);
//                        if(form.data('styled')) return false;
//
//                        form.closest('.ui-jqdialog').find('.ui-jqdialog-titlebar').wrapInner('<div class="widget-header" />')
//                        style_delete_form(form);
//
//                        form.data('styled', true);
//                    },
//                    onClick : function(e) {
//                        //alert(1);
//                    }
//                },
//                {
//                    //search form
//                    recreateForm: true,
//                    afterShowSearch: function(e){
//                        var form = $(e[0]);
//                        form.closest('.ui-jqdialog').find('.ui-jqdialog-title').wrap('<div class="widget-header" />')
//                        style_search_form(form);
//                    },
//                    afterRedraw: function(){
//                        style_ssearch_filters($(this));
//                    }
//                    ,
//                    multipleSearch: true,
//                    /**
//                     multipleGroup:true,
//                     showQuery: true
//                     */
//                },
//                {
//                    //view record form
//                    recreateForm: true,
//                    beforeShowForm: function(e){
//                        var form = $(e[0]);
//                        form.closest('.ui-jqdialog').find('.ui-jqdialog-title').wrap('<div class="widget-header" />')
//                    }
//                }
//        )



        function style_edit_form(form) {
            //enable datepicker on "sdate" field and switches for "stock" field
            form.find('input[name=sdate]').datepicker({format:'yyyy-mm-dd' , autoclose:true})

            form.find('input[name=stock]').addClass('ace ace-switch ace-switch-5').after('<span class="lbl"></span>');
            //don't wrap inside a label element, the checkbox value won't be submitted (POST'ed)
            //.addClass('ace ace-switch ace-switch-5').wrap('<label class="inline" />').after('<span class="lbl"></span>');


            //update buttons classes
            var buttons = form.next().find('.EditButton .fm-button');
            buttons.addClass('btn btn-sm').find('[class*="-icon"]').hide();//ui-icon, s-icon
            buttons.eq(0).addClass('btn-primary').prepend('<i class="ace-icon fa fa-check"></i>');
            buttons.eq(1).prepend('<i class="ace-icon fa fa-times"></i>')

            buttons = form.next().find('.navButton a');
            buttons.find('.ui-icon').hide();
            buttons.eq(0).append('<i class="ace-icon fa fa-chevron-left"></i>');
            buttons.eq(1).append('<i class="ace-icon fa fa-chevron-right"></i>');
        }

        function style_delete_form(form) {
            var buttons = form.next().find('.EditButton .fm-button');
            buttons.addClass('btn btn-sm btn-white btn-round').find('[class*="-icon"]').hide();//ui-icon, s-icon
            buttons.eq(0).addClass('btn-danger').prepend('<i class="ace-icon fa fa-trash-o"></i>');
            buttons.eq(1).addClass('btn-default').prepend('<i class="ace-icon fa fa-times"></i>')
        }

        function style_search_filters(form) {
            form.find('.delete-rule').val('X');
            form.find('.add-rule').addClass('btn btn-xs btn-primary');
            form.find('.add-group').addClass('btn btn-xs btn-success');
            form.find('.delete-group').addClass('btn btn-xs btn-danger');
        }
        function style_search_form(form) {
            var dialog = form.closest('.ui-jqdialog');
            var buttons = dialog.find('.EditTable')
            buttons.find('.EditButton a[id*="_reset"]').addClass('btn btn-sm btn-info').find('.ui-icon').attr('class', 'ace-icon fa fa-retweet');
            buttons.find('.EditButton a[id*="_query"]').addClass('btn btn-sm btn-inverse').find('.ui-icon').attr('class', 'ace-icon fa fa-comment-o');
            buttons.find('.EditButton a[id*="_search"]').addClass('btn btn-sm btn-purple').find('.ui-icon').attr('class', 'ace-icon fa fa-search');
        }

        function beforeDeleteCallback(e) {
            var form = $(e[0]);
            if(form.data('styled')) return false;

            form.closest('.ui-jqdialog').find('.ui-jqdialog-titlebar').wrapInner('<div class="widget-header" />')
            style_delete_form(form);

            form.data('styled', true);
        }

        function beforeEditCallback(e) {
            var form = $(e[0]);
            form.closest('.ui-jqdialog').find('.ui-jqdialog-titlebar').wrapInner('<div class="widget-header" />')
            style_edit_form(form);
        }



        //it causes some flicker when reloading or navigating grid
        //it may be possible to have some custom formatter to do this as the grid is being created to prevent this
        //or go back to default browser checkbox styles for the grid
        function styleCheckbox(table) {
            /**
             $(table).find('input:checkbox').addClass('ace')
             .wrap('<label />')
             .after('<span class="lbl align-top" />')


             $('.ui-jqgrid-labels th[id*="_cb"]:first-child')
             .find('input.cbox[type=checkbox]').addClass('ace')
             .wrap('<label />').after('<span class="lbl align-top" />');
             */
        }


        //unlike navButtons icons, action icons in rows seem to be hard-coded
        //you can change them like this in here if you want
        function updateActionIcons(table) {
            /**
             var replacement =
             {
                 'ui-ace-icon fa fa-pencil' : 'ace-icon fa fa-pencil blue',
                 'ui-ace-icon fa fa-trash-o' : 'ace-icon fa fa-trash-o red',
                 'ui-icon-disk' : 'ace-icon fa fa-check green',
                 'ui-icon-cancel' : 'ace-icon fa fa-times red'
             };
             $(table).find('.ui-pg-div span.ui-icon').each(function(){
						var icon = $(this);
						var $class = $.trim(icon.attr('class').replace('ui-icon', ''));
						if($class in replacement) icon.attr('class', 'ui-icon '+replacement[$class]);
					})
             */
        }

        //replace icons with FontAwesome icons like above
        function updatePagerIcons(table) {
            var replacement =
            {
                'ui-icon-seek-first' : 'ace-icon fa fa-angle-double-left bigger-140',
                'ui-icon-seek-prev' : 'ace-icon fa fa-angle-left bigger-140',
                'ui-icon-seek-next' : 'ace-icon fa fa-angle-right bigger-140',
                'ui-icon-seek-end' : 'ace-icon fa fa-angle-double-right bigger-140'
            };
            $('.ui-pg-table:not(.navtable) > tbody > tr > .ui-pg-button > .ui-icon').each(function(){
                var icon = $(this);
                var $class = $.trim(icon.attr('class').replace('ui-icon', ''));

                if($class in replacement) icon.attr('class', 'ui-icon '+replacement[$class]);
            })
        }

        function enableTooltips(table) {
            $('.navtable .ui-pg-button').tooltip({container:'body'});
            $(table).find('.ui-pg-div').tooltip({container:'body'});
        }

        //var selr = jQuery(grid_selector).jqGrid('getGridParam','selrow');

        $(document).one('ajaxloadstart.page', function(e) {
            $.jgrid.gridDestroy(grid_selector);
            $('.ui-jqdialog').remove();
        });
    });

    function editHotel(newsId) {
        var href = "${ctx}/news/edit.do?newsId=" + newsId;
        window.parent.aClieck2(href,"新闻资讯详情");
    }


</script>
</body>
</html>
