<%@ page language="java" import="java.util.*" pageEncoding="UTF-8" %>
<%@ include file="./header.jsp" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <meta charset="utf-8"/>
    <title>新闻资讯(编辑) - 谷里后台系统</title>

    <meta name="description" content="overview &amp; stats"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0"/>

    <!-- bootstrap & fontawesome -->
    <link rel="stylesheet" href="${ctx}/static/assets/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="${ctx}/static/assets/font-awesome/4.5.0/css/font-awesome.min.css"/>
    <link rel="stylesheet" href="${ctx}/static/assets/font-awesome/3.0/css/font-awesome.min.css"/>
    <link rel="stylesheet" href="${ctx}/static/assets/css/jquery-ui.min.css"/>
    <link rel="stylesheet" href="${ctx}/static/assets/css/bootstrap-duallistbox.min.css"/>
    <link rel="stylesheet" href="${ctx}/static/assets/css/bootstrap-datepicker3.min.css"/>
    <link rel="stylesheet" href="${ctx}/static/assets/css/ui.jqgrid.min.css"/>
    <link rel="stylesheet" href="${ctx}/static/wangEditor/css/wangEditor.css"/>
    <link rel="stylesheet" href="${ctx}/static/assets/css/select2.min.css"/>
    <!-- text fonts -->
    <link rel="stylesheet" href="${ctx}/static/assets/css/fonts.googleapis.com.css"/>

    <!-- ace styles -->
    <link rel="stylesheet" href="${ctx}/static/assets/css/ace.min.css"/>
    <!--[if IE 9]>
    <link rel="stylesheet" href="${ctx}/static/assets/css/ace-part2.min.css"/>
    <![endif]-->
    <link rel="stylesheet" href="${ctx}/static/assets/css/ace-rtl.min.css"/>

    <!--[if IE 9]>
    <link rel="stylesheet" href="${ctx}/static/assets/css/ace-ie.min.css"/>
    <![endif]-->

    <!-- HTML5shiv and Respond.js for IE8 to support HTML5 elements and media queries -->

    <!--[if IE 8]>
    <script src="${ctx}/static/assets/js/html5shiv.min.js"></script>
    <script src="${ctx}/static/assets/js/respond.min.js"></script>
    <![endif]-->


    <!--[if !IE]> -->
    <script src="${ctx}/static/assets/js/jquery-2.1.4.min.js"></script>
    <!-- <![endif]-->
    <!--[if IE]>
    <script src="${ctx}/static/assets/js/jquery-1.11.3.min.js"></script>
    <![endif]-->
    <script src="${ctx}/static/assets/js/bootstrap.min.js"></script>
    <script src="${ctx}/static/assets/js/ace-elements.min.js"></script>
    <script src="${ctx}/static/assets/js/ace.min.js"></script>
    <script src="${ctx}/static/assets/js/jquery.bootstrap-duallistbox.min.js"></script>
    <script src="${ctx}/static/assets/js/bootstrap-datepicker.min.js"></script>
    <script src="${ctx}/static/assets/js/jquery.jqGrid.min.js"></script>
    <script src="${ctx}/static/assets/js/grid.locale-en.js"></script>

    <link rel="stylesheet" type="text/css" href="${ctx}/static/layer/skin/default/layer.css"/>
    <script src="${ctx}/static/layer/layer.js"></script>
    <script src="${ctx}/static/js/public.js"></script>
    <script src="${ctx}/static/wangEditor/js/wangEditor.js"></script>
    <script src="${ctx}/static/assets/js/select2.min.js"></script>
    <style>
        .ui-jqdialog {
            margin-left: 30%;
        }

        #id {
            display: block;
        }

        .ui-th-column {
            text-align: center !important;
        }

        .page-content {
            padding: 30px 20px;
        }
    </style>
</head>

<body class="no-skin">


<div class="page-content">


    <div class="row">
        <div class="col-xs-12">
            <!-- PAGE CONTENT BEGINS -->
            <form class="form-horizontal" role="form" id="form" name="form" action="" enctype="multipart/form-data"
                  method="post">
                <input type="hidden" name="newsId" value="${news.newsId}">
                <div class="form-group">
                    <label class="col-sm-2 control-label no-padding-right" for="name"> 标题 </label>

                    <div class="col-sm-9">
                        <input type="text" id="name" name="title" value="${news.title}" placeholder="标题"
                               class="col-xs-10 col-sm-5">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label no-padding-right" for="description"> 简介 </label>

                    <div class="col-sm-9">
                        <c:if test="${not empty news.intro}">
                            <textarea id="description" name="intro" class="col-xs-10 col-sm-5">${news.intro}</textarea>
                        </c:if>
                        <c:if test="${empty news.intro}">
                            <textarea id="description" name="intro" class="col-xs-10 col-sm-5"></textarea>
                        </c:if>

                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label no-padding-right"> 图片 </label>

                    <div class="col-sm-9">
                        <c:if test="${!empty news.pic}">
                            <img src="${news.pic}" id="newsPic" style="width:88px;height:88px;border:1px solid #ddd;"/>
                        </c:if>
                        <c:if test="${empty news.pic}">
                            <img src="${ctx}/static/img/no_pic.png" id="newsPic"
                                 style="width:88px;height:88px;border:1px solid #ddd;"/>
                        </c:if>
                        <input type="file" name="news_pic" id=news_pic" style="width:540px;height:22px;cursor:hand"
                               class="fileinput"
                               onchange="checkImgType(this);setPreview(this,'newsPic','sm_div',88,88);"/>(原始尺寸图片160*160)
                    </div>
                </div>
                <div class="space-4"></div>


                <div class="form-group">
                    <label class="col-sm-2 control-label no-padding-right"> 发布时间 </label>

                    <div class="col-sm-4">
                        <div class="input-group input-group-sm">
                            <input type="text" value="${publishTime}" name="publishTime" id="datepicker"
                                   class="form-control hasDatepicker">
													<span class="input-group-addon">
														<i class="ace-icon fa fa-calendar"></i>
													</span>
                        </div>
                    </div>
                </div>

                <div class="space-4"></div>


                <div class="clearfix"></div>


                <div class="form-group">
                    <label class="col-sm-2 control-label no-padding-right" for="description"> 文章: </label>
                    <div class="col-sm-9" style="margin-top: 10px">
                        <div id="div1" style="height:400px;max-height:500px;">
                            <c:if test="${!empty news.text}">
                                ${news.text}
                            </c:if>
                            <c:if test="${empty news.text}">
                                <p>请输入内容...</p>
                            </c:if>

                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>

                <div class="space-4"></div>

                <div class="align-center">
                    <button class="btn btn-info" type="button" id="submit_btn">
                        <i class="ace-icon fa fa-check bigger-110"></i>
                        提交
                    </button>
                </div>
            </form>
            <!-- <div class="hr hr-18 dotted hr-double"></div> -->
            <!-- PAGE CONTENT ENDS -->
        </div><!-- /.col -->
    </div><!-- /.row -->
</div><!-- /.page-content -->

<script type="text/javascript">
    jQuery(function ($) {
        var editor = new wangEditor('div1');
        editor.config.uploadImgUrl = '${ctx}/common/upload.do';
        editor.create();

        $('.select2').css('width', '200px').select2({allowClear: true})
        $('#select2-multiple-style .btn').on('click', function (e) {
            var target = $(this).find('input[type=radio]');
            var which = parseInt(target.val());
            if (which == 2) $('.select2').addClass('tag-input-style');
            else $('.select2').removeClass('tag-input-style');
        });

        $("#datepicker").datepicker({
            autoclose: true,//选中之后自动隐藏日期选择框
            clearBtn: true,//清除按钮
            todayBtn: 'linked',//今日按钮
            language: "zh-CN",
            format: "yyyy-mm-dd"//日期格式，详见 http://bootstrap-datepicker.readthedocs.org/en/release/options.html#format
        });


//        $( "#datepicker" ).datepicker({
//            showOtherMonths: true,
//            selectOtherMonths: false,
//            //isRTL:true,
//
//
//            /*
//             changeMonth: true,
//             changeYear: true,
//
//             showButtonPanel: true,
//             beforeShow: function() {
//             //change button colors
//             var datepicker = $(this).datepicker( "widget" );
//             setTimeout(function(){
//             var buttons = datepicker.find('.ui-datepicker-buttonpane')
//             .find('button');
//             buttons.eq(0).addClass('btn btn-xs');
//             buttons.eq(1).addClass('btn btn-xs btn-success');
//             buttons.wrapInner('<span class="bigger-110" />');
//             }, 0);
//             }
//             */
//        });

        $("#submit_btn").click(function () {
            var form = document.form;

            // 获取编辑器区域完整html代码
            var html = editor.$txt.html();


//            // 获取编辑器纯文本内容
//            var text = editor.$txt.text();
//
//            // 获取格式化后的纯文本
//            var formatText = editor.$txt.formatText();

            var temp = document.createElement("input"); //text input
            temp.setAttribute("name", "text");
            temp.setAttribute("value", html);
            temp.setAttribute("type", "hidden");
            form.appendChild(temp);


            form.action = "${ctx}/news/add.do";
            form.submit();
            return false;
        })
    });


</script>
</body>
</html>
