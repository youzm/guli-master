<%@ page language="java" import="java.util.*" pageEncoding="UTF-8" %>
<%@ include file="./header.jsp" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <meta charset="utf-8"/>
    <title>广告位(新增) - 谷里后台系统</title>

    <meta name="description" content="overview &amp; stats"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0"/>

    <!-- bootstrap & fontawesome -->
    <link rel="stylesheet" href="${ctx}/static/assets/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="${ctx}/static/assets/font-awesome/4.5.0/css/font-awesome.min.css"/>
    <link rel="stylesheet" href="${ctx}/static/assets/font-awesome/3.0/css/font-awesome.min.css"/>
    <link rel="stylesheet" href="${ctx}/static/assets/css/jquery-ui.min.css" />
    <link rel="stylesheet" href="${ctx}/static/assets/css/bootstrap-duallistbox.min.css" />
    <link rel="stylesheet" href="${ctx}/static/assets/css/bootstrap-datepicker3.min.css" />
    <link rel="stylesheet" href="${ctx}/static/assets/css/ui.jqgrid.min.css" />
    <link rel="stylesheet" href="${ctx}/static/wangEditor/css/wangEditor.css" />
    <!-- text fonts -->
    <link rel="stylesheet" href="${ctx}/static/assets/css/fonts.googleapis.com.css"/>

    <!-- ace styles -->
    <link rel="stylesheet" href="${ctx}/static/assets/css/ace.min.css"/>
    <!--[if IE 9]>
    <link rel="stylesheet" href="${ctx}/static/assets/css/ace-part2.min.css"/>
    <![endif]-->
    <link rel="stylesheet" href="${ctx}/static/assets/css/ace-rtl.min.css"/>

    <!--[if IE 9]>
    <link rel="stylesheet" href="${ctx}/static/assets/css/ace-ie.min.css"/>
    <![endif]-->

    <!-- HTML5shiv and Respond.js for IE8 to support HTML5 elements and media queries -->

    <!--[if IE 8]>
    <script src="${ctx}/static/assets/js/html5shiv.min.js"></script>
    <script src="${ctx}/static/assets/js/respond.min.js"></script>
    <![endif]-->


    <!--[if !IE]> -->
    <script src="${ctx}/static/assets/js/jquery-2.1.4.min.js"></script>
    <!-- <![endif]-->
    <!--[if IE]>
    <script src="${ctx}/static/assets/js/jquery-1.11.3.min.js"></script>
    <![endif]-->
    <script src="${ctx}/static/assets/js/bootstrap.min.js"></script>
    <script src="${ctx}/static/assets/js/ace-elements.min.js"></script>
    <script src="${ctx}/static/assets/js/ace.min.js"></script>
    <script src="${ctx}/static/assets/js/jquery.bootstrap-duallistbox.min.js"></script>
    <script src="${ctx}/static/assets/js/bootstrap-datepicker.min.js"></script>
    <script src="${ctx}/static/assets/js/jquery.jqGrid.min.js"></script>
    <script src="${ctx}/static/assets/js/grid.locale-en.js"></script>

    <link rel="stylesheet" type="text/css" href="${ctx}/static/layer/skin/default/layer.css"/>
    <script src="${ctx}/static/layer/layer.js"></script>
    <script src="${ctx}/static/js/public.js"></script>
    <script src="${ctx}/static/wangEditor/js/wangEditor.js"></script>
    <style>
        .ui-jqdialog {
            margin-left: 30%;
        }
        #id{
            display:block;
        }
        .ui-th-column {
            text-align: center !important;
        }
        .page-content {
            padding:30px 20px;
        }
    </style>
</head>

<body class="no-skin">


<div class="page-content">
    <div class="row">
        <div class="col-xs-12">
            <!-- PAGE CONTENT BEGINS -->
            <form class="form-horizontal" role="form" id="form" name="form" action="" enctype="multipart/form-data" method="post">
                <input type="hidden" name="bannerId" value="${banner.bannerId}">

                <div class="form-group">
                    <label class="col-sm-2 control-label no-padding-right" > 图片 </label>

                    <div class="col-sm-9">
                        <c:if test="${!empty banner.pic}">
                            <img src="${banner.pic}" id="banner_pic" style="width:650px;height:288px;border:1px solid #ddd;"/>
                        </c:if>
                        <c:if test="${empty banner.pic}">
                            <img src="${ctx}/static/img/no_pic.png" id="banner_pic" style="width:650px;height:288px;border:1px solid #ddd;"/>
                        </c:if>
                        <input type="file" name="banner_pic" id=bannerPic" style="width:540px;height:22px;cursor:hand"
                               class="fileinput" onchange="checkImgType(this);setPreview(this,'banner_pic','sm_div',650,288);"/>(原始尺寸图片650*288)
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label no-padding-right" for="url"> 超链接  </label>

                    <div class="col-sm-9">
                        <input type="text" id="url" name="url" value="${banner.url}" placeholder="景点名称" class="col-xs-10 col-sm-5">
                    </div>
                </div>

                <%--<div class="form-group">--%>
                    <%--<label class="col-sm-3 control-label no-padding-right" for="title"> 排序 </label>--%>

                    <%--<div class="col-sm-9">--%>
                        <%--<input type="text" id="title" name="title" value="${spot.title}" placeholder="副标题" class="col-xs-10 col-sm-5">--%>
                    <%--</div>--%>
                <%--</div>--%>
                <div class="space-4"></div>

                <div class="align-center">
                    <button class="btn btn-info" type="button" id="submit_btn">
                        <i class="ace-icon fa fa-check bigger-110"></i>
                        提交
                    </button>
                </div>
            </form>
            <!-- <div class="hr hr-18 dotted hr-double"></div> -->
            <!-- PAGE CONTENT ENDS -->
        </div><!-- /.col -->
    </div><!-- /.row -->
</div><!-- /.page-content -->

<script type="text/javascript">
    jQuery(function($) {
        <%--var editor = new wangEditor('div1');--%>
        <%--editor.config.uploadImgUrl = '${ctx}/common/upload.do';--%>
        <%--editor.create();--%>

        $("#submit_btn").click(function(){
//            var form = document.form;
//
//            // 获取编辑器区域完整html代码
//            var html = editor.$txt.html();
//
//
//
////            // 获取编辑器纯文本内容
////            var text = editor.$txt.text();
////
////            // 获取格式化后的纯文本
////            var formatText = editor.$txt.formatText();
//
//            var temp = document.createElement("input") ; //text input
//            temp.setAttribute("name", "text");
//            temp.setAttribute("value", html);
//            temp.setAttribute("type","hidden");
//            form.appendChild(temp);


            form.action = "${ctx}/farmStay/banner/add.do";
            form.submit();
            return false;
        })
    });


</script>
</body>
</html>
