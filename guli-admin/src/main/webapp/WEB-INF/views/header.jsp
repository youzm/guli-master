<%@ page import="com.carl.guli.common.constant.Constant" %>
<%@ page import="com.carl.guli.model.AccountInfo" %>
<%@ page language="java" pageEncoding="UTF-8" %>
<%
    String path = request.getContextPath();


    String serverPath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort();
    String basePath = null;
    if (request.getServerPort() == 80) {
        basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
    } else {
        basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
    }


    response.setHeader("pragam", "no-cache");
    response.setHeader("Cache-Control", "no-cache");
    response.setDateHeader("expires", -1);
    request.setAttribute("ctx", request.getContextPath());

//	request.setAttribute("imgurl", "http://127.0.0.1:8081/files/vegetable");
//	request.setAttribute("imgurl", "http://www.byny88.com/ROOT/files/vegetable");


    if (request.getSession() != null && request.getSession().getAttribute("cache_account") != null) {
        AccountInfo account = (AccountInfo) session.getAttribute(Constant.CACHE_ACCOUNT);
        request.setAttribute("account_departmId", account.getDepartmId());
    }
%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>