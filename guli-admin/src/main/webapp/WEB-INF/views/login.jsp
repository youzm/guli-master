<%@ page language="java" import="java.util.*" pageEncoding="UTF-8" %>
<%@ page import="java.net.URLDecoder" %>
<%@ include file="./header.jsp" %>
<%
    String password="";
    String name="";
    String checked="";
    Cookie[] cookies = request.getCookies();        //取出cookie对象组
    for(int i = 0; cookies != null && i < cookies.length;i++){
        Cookie cookie = cookies[i];       //  取出其中的一个对象，含有name ,value
        if(cookie != null && "name".equals(cookie.getName())){      //获取第一个cookie对象的name
            name = URLDecoder.decode(cookie.getValue(), "UTF-8");//进行解码
            checked = "checked";
        }
        if(cookie != null && "password".equals(cookie.getName())){
            password = cookie.getValue();
        }
    }
%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <meta charset="utf-8"/>
    <title>登录界面 - 谷里后台系统</title>

    <meta name="description" content="User login page"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0"/>

    <!-- bootstrap & fontawesome -->
    <link rel="stylesheet" href="${ctx}/static/assets/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="${ctx}/static/assets/font-awesome/4.5.0/css/font-awesome.min.css"/>

    <!-- text fonts -->
    <link rel="stylesheet" href="${ctx}/static/assets/css/fonts.googleapis.com.css"/>

    <!-- ace styles -->
    <link rel="stylesheet" href="${ctx}/static/assets/css/ace.min.css"/>
    <!--[if IE 9]>
    <link rel="stylesheet" href="${ctx}/static/assets/css/ace-part2.min.css"/>
    <![endif]-->
    <link rel="stylesheet" href="${ctx}/static/assets/css/ace-rtl.min.css"/>

    <!--[if IE 9]>
    <link rel="stylesheet" href="${ctx}/static/assets/css/ace-ie.min.css"/>
    <![endif]-->

    <!-- HTML5shiv and Respond.js for IE8 to support HTML5 elements and media queries -->

    <!--[if IE 8]>
    <script src="${ctx}/static/assets/js/html5shiv.min.js"></script>
    <script src="${ctx}/static/assets/js/respond.min.js"></script>
    <![endif]-->
    <style>
        .login-layout {
            background-image: url(${ctx}/static/assets/images/login_bg.jpg);
            background-size: cover;
        }

        .login-layout .widget-box {
            background-color:rgba(0,0,0,.15);
        }

        .position-relative {
            margin-top: 20px;
        }

        .login-container h1{
            display:block;
            margin:100px auto 0;
            width:100%;
            height:66px;
            background:url(${ctx}/static/assets/images/login_title.png) no-repeat;
            background-size: contain;
        }
    </style>
</head>

<body class="login-layout" onkeydown="keyLogin();">
<div class="main-container">
    <div class="main-content">
        <div class="row">
            <div class="col-sm-10 col-sm-offset-1">
                <div class="login-container">
                    <div class="center">
                        <h1>
                            <%--<i class="ace-icon fa fa-leaf green"></i>--%>
                            <%--<span style="color: burlywood;font-family:'隶书'" id="id-text2"></span>--%>
                        </h1>
                    </div>

                    <div class="space-6"></div>

                    <div class="position-relative">
                        <div id="login-box" class="login-box visible widget-box no-border">
                            <div class="widget-body">
                                <div class="widget-main">
                                    <h4 class="header blue lighter bigger">
                                        <i class="ace-icon fa fa-coffee green"></i>
                                        请输入你的登录账号
                                    </h4>

                                    <div class="space-6"></div>

                                    <form method="post">
                                        <fieldset>
                                            <label class="block clearfix">
														<span class="block input-icon input-icon-right">
															<input type="text" class="form-control"  placeholder="用户名"
                                                     l              id="job_number" value="<%=name%>"/>
															<i class="ace-icon fa fa-user"></i>
														</span>
                                            </label>

                                            <label class="block clearfix">
														<span class="block input-icon input-icon-right">
															<input type="password" class="form-control" placeholder="密码"
                                                                   id="password" value="<%=password%>"/>
															<i class="ace-icon fa fa-lock"></i>
														</span>
                                            </label>

                                            <div class="space"></div>

                                            <div class="clearfix">
                                                <label class="inline">
                                                    <input type="checkbox" class="ace" value="yes" <%=checked%> id="remember"/>
                                                    <span class="lbl"> 记住我</span>
                                                </label>

                                                <button type="button" id="btn_login" class="width-35 pull-right btn btn-sm btn-success"
                                                        onclick="login()">
                                                    <i class="ace-icon fa fa-key"></i>
                                                    <span class="bigger-110">登录</span>
                                                </button>
                                            </div>

                                            <div class="space-4"></div>
                                        </fieldset>
                                    </form>

                                    <!--<div class="social-or-login center">-->
                                    <!--<span class="bigger-110">Or Login Using</span>-->
                                    <!--</div>-->

                                    <!--<div class="space-6"></div>-->

                                    <!--<div class="social-login center">-->
                                    <!--<a class="btn btn-primary">-->
                                    <!--<i class="ace-icon fa fa-facebook"></i>-->
                                    <!--</a>-->

                                    <!--<a class="btn btn-info">-->
                                    <!--<i class="ace-icon fa fa-twitter"></i>-->
                                    <!--</a>-->

                                    <!--<a class="btn btn-danger">-->
                                    <!--<i class="ace-icon fa fa-google-plus"></i>-->
                                    <!--</a>-->
                                    <!--</div>-->
                                </div><!-- /.widget-main -->

                                <div class="toolbar clearfix">
                                    <!--<div>-->
                                    <!--<a href="#" data-target="#forgot-box" class="forgot-password-link">-->
                                    <!--<i class="ace-icon fa fa-arrow-left"></i>-->
                                    <!--I forgot my password-->
                                    <!--</a>-->
                                    <!--</div>-->

                                    <!--<div>-->
                                    <!--<a href="#" data-target="#signup-box" class="user-signup-link">-->
                                    <!--I want to register-->
                                    <!--<i class="ace-icon fa fa-arrow-right"></i>-->
                                    <!--</a>-->
                                    <!--</div>-->

                                </div>


                            </div><!-- /.widget-body -->
                        </div><!-- /.login-box -->

                    </div><!-- /.position-relative -->
                </div>
            </div><!-- /.col -->
            <%--<div style=" position: fixed;bottom: 0px;text-align: center;" class="col-sm-12">--%>
                <%--<h4 class="white" id="id-company-text">&copy; 江苏科能电力工程咨询有限公司</h4>--%>
                <%--<h6 class="white">地址:江苏省南京市渡江路10号</h6>--%>
            <%--</div>--%>
        </div><!-- /.row -->
    </div><!-- /.main-content -->
</div><!-- /.main-container -->

<!-- basic scripts -->

<!--[if !IE]> -->
<script src="${ctx}/static/assets/js/jquery-2.1.4.min.js"></script>

<!-- <![endif]-->

<!--[if IE]>
<script src="${ctx}/static/assets/js/jquery-1.11.3.min.js"></script>
<![endif]-->
<link rel="stylesheet" type="text/css" href="${ctx}/static/layer/skin/default/layer.css"/>
<script src="${ctx}/static/layer/layer.js"></script>
<script type="text/javascript">
    if ('ontouchstart' in document.documentElement) document.write("<script src='assets/js/jquery.mobile.custom.min.js'>" + "<" + "/script>");
</script>

<!-- inline scripts related to this page -->
<script type="text/javascript">
    (function ($) {
        $(document).on('click', '.toolbar a[data-target]', function (e) {
            e.preventDefault();
            var target = $(this).data('target');
            $('.widget-box.visible').removeClass('visible');//hide others
            $(target).addClass('visible');//show target
        });
    });


    //you don't need this, just used for changing background
    (function ($) {
        $('#btn-login-dark').on('click', function (e) {
            $('body').attr('class', 'login-layout');
            $('#id-text2').attr('class', 'white');
            $('#id-company-text').attr('class', 'blue');

            e.preventDefault();
        });
        $('#btn-login-light').on('click', function (e) {
            $('body').attr('class', 'login-layout light-login');
            $('#id-text2').attr('class', 'grey');
            $('#id-company-text').attr('class', 'blue');

            e.preventDefault();
        });
        $('#btn-login-blur').on('click', function (e) {
            $('body').attr('class', 'login-layout blur-login');
            $('#id-text2').attr('class', 'white');
            $('#id-company-text').attr('class', 'light-blue');

            e.preventDefault();
        });

    });

    function login() {
        var jobNumber = $("#job_number").val().trim();
        var password = $("#password").val();
        var remember = $("#remember").val();
        $.post("${ctx}/login.do", {jobNumber: jobNumber, password: password,remember:remember}, function (resp) {
            if (resp.resultCode == 1) {
                window.location.href = "${ctx}/dashboard.do";
            }
            if (resp.resultCode == 0) {
                layer.alert(resp.resultMsg, {icon: 2});
            }
        });
//				window.location.href = "index.html";
    }

    function keyLogin(){
        if (event.keyCode==13)  //回车键的键值为13
            document.getElementById("btn_login").click(); //调用登录按钮的登录事件
    }
</script>
</body>
</html>
