<%@ page language="java" import="java.util.*" pageEncoding="UTF-8" %>
<%@ include file="./header.jsp" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <meta charset="utf-8"/>
    <title>密码修改 - 谷里后台系统</title>

    <meta name="description" content="overview &amp; stats"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0"/>

    <!-- bootstrap & fontawesome -->
    <link rel="stylesheet" href="${ctx}/static/assets/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="${ctx}/static/assets/font-awesome/4.5.0/css/font-awesome.min.css"/>
    <link rel="stylesheet" href="${ctx}/static/assets/font-awesome/3.0/css/font-awesome.min.css"/>
    <link rel="stylesheet" href="${ctx}/static/assets/css/jquery-ui.min.css" />
    <link rel="stylesheet" href="${ctx}/static/assets/css/bootstrap-duallistbox.min.css" />
    <link rel="stylesheet" href="${ctx}/static/assets/css/bootstrap-datepicker3.min.css" />
    <link rel="stylesheet" href="${ctx}/static/assets/css/ui.jqgrid.min.css" />
    <link rel="stylesheet" href="${ctx}/static/wangEditor/css/wangEditor.css" />
    <link rel="stylesheet" href="${ctx}/static/assets/css/select2.min.css" />
    <!-- text fonts -->
    <link rel="stylesheet" href="${ctx}/static/assets/css/fonts.googleapis.com.css"/>

    <!-- ace styles -->
    <link rel="stylesheet" href="${ctx}/static/assets/css/ace.min.css"/>
    <!--[if IE 9]>
    <link rel="stylesheet" href="${ctx}/static/assets/css/ace-part2.min.css"/>
    <![endif]-->
    <link rel="stylesheet" href="${ctx}/static/assets/css/ace-rtl.min.css"/>

    <!--[if IE 9]>
    <link rel="stylesheet" href="${ctx}/static/assets/css/ace-ie.min.css"/>
    <![endif]-->

    <!-- HTML5shiv and Respond.js for IE8 to support HTML5 elements and media queries -->

    <!--[if IE 8]>
    <script src="${ctx}/static/assets/js/html5shiv.min.js"></script>
    <script src="${ctx}/static/assets/js/respond.min.js"></script>
    <![endif]-->


    <!--[if !IE]> -->
    <script src="${ctx}/static/assets/js/jquery-2.1.4.min.js"></script>
    <!-- <![endif]-->
    <!--[if IE]>
    <script src="${ctx}/static/assets/js/jquery-1.11.3.min.js"></script>
    <![endif]-->
    <script src="${ctx}/static/assets/js/bootstrap.min.js"></script>
    <script src="${ctx}/static/assets/js/ace-elements.min.js"></script>
    <script src="${ctx}/static/assets/js/ace.min.js"></script>
    <script src="${ctx}/static/assets/js/jquery.bootstrap-duallistbox.min.js"></script>
    <script src="${ctx}/static/assets/js/bootstrap-datepicker.min.js"></script>
    <script src="${ctx}/static/assets/js/jquery.jqGrid.min.js"></script>
    <script src="${ctx}/static/assets/js/grid.locale-en.js"></script>

    <link rel="stylesheet" type="text/css" href="${ctx}/static/layer/skin/default/layer.css"/>
    <script src="${ctx}/static/layer/layer.js"></script>
    <script src="${ctx}/static/js/public.js"></script>
    <script src="${ctx}/static/wangEditor/js/wangEditor.js"></script>
    <script src="${ctx}/static/assets/js/select2.min.js"></script>
    <style>
        .ui-jqdialog {
            margin-left: 30%;
        }
        #id{
            display:block;
        }
        .ui-th-column {
            text-align: center !important;
        }
    </style>
</head>

<body class="no-skin">


<div class="page-content">



    <div class="row">
        <div class="col-xs-12">
            <!-- PAGE CONTENT BEGINS -->
            <form class="form-horizontal" role="form" id="form" name="form" action="" enctype="multipart/form-data" method="post">
                <input type="hidden" name="goodId" value="${good.goodId}">
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right" for="name"> 特产名称 </label>

                    <div class="col-sm-9">
                        <input type="text" id="name" name="name" value="${good.name}" placeholder="特产名称" class="col-xs-10 col-sm-5">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right" for="name"> 特产简介 </label>

                    <div class="col-sm-9">
                        <input type="text" id="intro" name="intro" value="${good.intro}" placeholder="特产简介" class="col-xs-10 col-sm-5">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right" for="name"> 价格 </label>
                    <div class="col-sm-9">
                        <input type="text"  name="price" value="${good.price}" placeholder="价格" class="col-xs-10 col-sm-5">
                        <input type="text"  name="unit" value="${good.unit}" placeholder="单位" class="col-xs-10 col-sm-5">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right"> 特产图标 </label>

                    <div class="col-sm-9">
                        <c:if test="${!empty good.goodPic}">
                            <img src="${good.goodPic}" id="goodPic" style="width:88px;height:88px;border:1px solid #ddd;"/>
                        </c:if>
                        <c:if test="${empty good.goodPic}">
                            <img src="${ctx}/static/img/no_pic.png" id="goodPic" style="width:88px;height:88px;border:1px solid #ddd;"/>
                        </c:if>
                        <input type="file" name="good_pic" id=good_pic" style="width:540px;height:22px;cursor:hand"
                               class="fileinput" onchange="checkImgType(this);setPreview(this,'goodPic','sm_div',88,88);"/>(原始尺寸图片160*160)
                    </div>
                </div>

                <div class="space-4"></div>

                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right" for="name"> 坐标 </label>
                    <div class="col-sm-9">
                        <input type="text"  name="location" value="${good.location}" placeholder="(如:32.179100,118.600159)" class="col-xs-10 col-sm-5">
                    </div>
                </div>

                <div class="space-4"></div>

                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right" for="name"> 地址 </label>
                    <div class="col-sm-9">
                        <input type="text"  name="address" value="${good.address}" placeholder="地址" class="col-xs-10 col-sm-5">
                    </div>
                </div>

                <div class="space-4"></div>

                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right" for="name"> 联系人 </label>
                    <div class="col-sm-9">
                        <input type="text"  name="contactor" value="${good.contactor}" placeholder="联系人" class="col-xs-10 col-sm-5">
                    </div>
                </div>

                <div class="space-4"></div>

                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right" for="name"> 电话号码 </label>
                    <div class="col-sm-9">
                        <input type="text"  name="phone" value="${good.phone}" placeholder="电话号码" class="col-xs-10 col-sm-5">
                    </div>
                </div>

                <div class="space-4"></div>

                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right"> 滚动图1 </label>

                    <div class="col-sm-9">
                        <c:if test="${!empty good.banner1}">
                            <img src="${good.banner1}" id="banner_pic1" style="width:75px;height:65px;border:1px solid #ddd;"/>
                        </c:if>
                        <c:if test="${empty good.banner1}">
                            <img src="${ctx}/static/img/no_pic.png" id="banner_pic1" style="width:75px;height:65px;border:1px solid #ddd;"/>
                        </c:if>
                        <input type="file" name="banner_1" id=bannerPic1" style="width:540px;height:22px;cursor:hand"
                               class="fileinput" multiple onchange="checkImgType(this);setPreview(this,'banner_pic1','sm_div',75,65);"/>(原始尺寸图片750*650)
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right"> 滚动图2 </label>

                    <div class="col-sm-9">
                        <c:if test="${!empty good.banner2}">
                            <img src="${good.banner2}" id="banner_pic2" style="width:75px;height:65px;border:1px solid #ddd;"/>
                        </c:if>
                        <c:if test="${empty good.banner2}">
                            <img src="${ctx}/static/img/no_pic.png" id="banner_pic2" style="width:75px;height:65px;border:1px solid #ddd;"/>
                        </c:if>                        <input type="file" name="banner_2" id=bannerPic2" style="width:540px;height:22px;cursor:hand"
                               class="fileinput" multiple onchange="checkImgType(this);setPreview(this,'banner_pic2','sm_div',75,65);"/>(原始尺寸图片750*650)
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right"> 滚动图3 </label>

                    <div class="col-sm-9">
                        <c:if test="${!empty good.banner3}">
                            <img src="${good.banner3}" id="banner_pic3" style="width:75px;height:65px;border:1px solid #ddd;"/>
                        </c:if>
                        <c:if test="${empty good.banner3}">
                            <img src="${ctx}/static/img/no_pic.png" id="banner_pic3" style="width:75px;height:65px;border:1px solid #ddd;"/>
                        </c:if>                        <input type="file" name="banner_3" id=bannerPic3" style="width:540px;height:22px;cursor:hand"
                               class="fileinput" multiple onchange="checkImgType(this);setPreview(this,'banner_pic3','sm_div',75,65);"/>(原始尺寸图片750*650)
                    </div>
                </div>

                <div class="clearfix"></div>

                <div class="form-group">
                    <%--<label class="col-sm-3 control-label no-padding-right" for="description"> 图文详情 </label>--%>
                    <div  id="div1" style="height:400px;max-height:500px;">
                        <c:if test="${!empty good.text}">
                            ${good.text}
                        </c:if>
                        <c:if test="${empty good.text}">
                            <p>请输入内容...</p>
                        </c:if>

                    </div>
                </div>

                <div class="space-4"></div>

                <div class="clearfix form-actions">
                    <div class="col-md-offset-3 col-md-9">
                        <button class="btn btn-info" type="button" id="submit_btn">
                            <i class="ace-icon fa fa-check bigger-110"></i>
                            提交
                        </button>
                    </div>
                </div>
            </form>
            <div class="hr hr-18 dotted hr-double"></div>
            <!-- PAGE CONTENT ENDS -->
        </div><!-- /.col -->
    </div><!-- /.row -->
</div><!-- /.page-content -->

<script type="text/javascript">
    jQuery(function($) {
        var editor = new wangEditor('div1');
        editor.config.uploadImgUrl = '${ctx}/common/upload.do';
        editor.create();

        $('.select2').css('width','200px').select2({allowClear:true})
        $('#select2-multiple-style .btn').on('click', function(e){
            var target = $(this).find('input[type=radio]');
            var which = parseInt(target.val());
            if(which == 2) $('.select2').addClass('tag-input-style');
            else $('.select2').removeClass('tag-input-style');
        });

        $("#submit_btn").click(function(){
            var form = document.form;

            // 获取编辑器区域完整html代码
            var html = editor.$txt.html();



//            // 获取编辑器纯文本内容
//            var text = editor.$txt.text();
//
//            // 获取格式化后的纯文本
//            var formatText = editor.$txt.formatText();

            var temp = document.createElement("input") ; //text input
            temp.setAttribute("name", "text");
            temp.setAttribute("value", html);
            temp.setAttribute("type","hidden");
            form.appendChild(temp);


            form.action = "${ctx}/good/add.do";
            form.submit();
            return false;
        })
    });


</script>
</body>
</html>
