<%@ page language="java" import="java.util.*" pageEncoding="UTF-8" %>
<%@ include file="./header.jsp" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <meta charset="utf-8"/>
    <title>未审核工日 - 工日系统</title>

    <meta name="description" content="overview &amp; stats"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0"/>

    <link rel="stylesheet" href="${ctx}/static/assets/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="assets/font-awesome/4.5.0/css/font-awesome.min.css" />
    <link rel="stylesheet" href="${ctx}/static/assets/font-awesome/3.0/css/font-awesome.min.css"/>
    <link rel="stylesheet" href="assets/css/jquery-ui.min.css" />
    <link rel="stylesheet" href="${ctx}/static/datepicker/css/bootstrap-datepicker.min.css" />
    <link rel="stylesheet" href="${ctx}/static/assets/css/ui.jqgrid.min.css" />
    <link rel="stylesheet" href="assets/css/ace.min.css" class="ace-main-stylesheet" id="main-ace-style" />
    <link rel="stylesheet" href="assets/css/ace-skins.min.css" />
    <link rel="stylesheet" href="assets/css/ace-rtl.min.css" />
    <link rel="stylesheet" type="text/css" href="layer/skin/default/layer.css"/>


    <script src="assets/js/jquery-1.11.3.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/ace-elements.min.js"></script>
    <script src="assets/js/ace.min.js"></script>
    <script src="layer/layer.js"></script>

    <script src="${ctx}/static/datepicker/js/bootstrap-datepicker.js"></script>
    <script src="${ctx}/static/datepicker/locales/bootstrap-datepicker.zh-CN.min.js"></script>

    <script src="assets/js/jquery.jqGrid.min.js"></script>
    <script src="assets/js/grid.locale-en.js"></script>
    <style>
        .ui-jqdialog {
            margin-left: 30%;
        }
        #id{
            display:block;
        }
        .nav-search {
            margin-right: 20px;
        }
    </style>
</head>

<body class="no-skin">


<div class="page-content">
    <div class="row">
        <div class="col-xs-12">
            <div class="col-xs-10 col-sm-push-1">
                <h4>未审核工日:</h4>
            </div>
            <div class="row col-xs-10 col-sm-push-1 panel panel-default" style="background: #f9fdff;padding: 10px;">
                <div>
                    <%--<div class="nav-search" id="nav-search" style="line-height: 40px;z-index: 2">--%>
                        <%--<form class="form-search" id="form-search">--%>
								<%--<span class="input-icon">--%>
									<%--<input type="text" placeholder="选择填报月份" class="nav-search-input" id="nav-search-input" autocomplete="off">--%>
									<%--&lt;%&ndash;<i class="ace-icon fa fa-search nav-search-icon" style="top:7px"></i>&ndash;%&gt;--%>
								<%--</span>--%>
                        <%--</form>--%>
                    <%--</div>--%>
                    <table id="grid-table"></table>
                    <div id="grid-pager"></div>
                </div><!-- /.span -->
                <%--<button class="btn btn-danger" onclick="submitHours()">提交工日</button>--%>
            </div><!-- /.row -->

        </div>
    </div>
    <div class="hr hr-18 dotted hr-double"></div>
</div><!-- /.page-content -->

<script type="text/javascript">

    jQuery(function($) {
        var grid_selector = "#grid-table";
        var pager_selector = "#grid-pager";

        var parent_column = $(grid_selector).closest('[class*="col-"]');
        //resize to fit page size
        $(window).on('resize.jqGrid', function () {
            $(grid_selector).jqGrid( 'setGridWidth', parent_column.width() );
        })

        //resize on sidebar collapse/expand
        $(document).on('settings.ace.jqGrid' , function(ev, event_name, collapsed) {
            if( event_name === 'sidebar_collapsed' || event_name === 'main_container_fixed' ) {
                //setTimeout is for webkit only to give time for DOM changes and then redraw!!!
                setTimeout(function() {
                    $(grid_selector).jqGrid( 'setGridWidth', parent_column.width() );
                }, 20);
            }
        });

        jQuery(grid_selector).jqGrid({
            url:"${ctx}/hours/history?status=1",
            datatype:"json",
            mtype:"POST",
            height:"auto",
            autowidth:true,
            colNames:['工程信息','标准工日代码','工程类型','电压等级','类别', '设计阶段', '专业', '承担角色','卷册','状态','月份', '我的申报'],
            colModel:[
                {name: 'proInfo', index: '', width: '250%', fixed: true, sortable: false, resize: false,
                    formatter: function(cellvalue, options, rowObject) {
                        return rowObject.project.projName + "("  + rowObject.project.projCode + ")";
                    }
                },
                {name:'projectDetail.code',index:'code', editable:true,width:'25%'},
                {name:'projectDetail.proType',index:'proType',width:'25%'},
                {name:'projectDetail.eleLevel',index:'eleLevel',width:'20%'},
                {name:'projectDetail.category',index:'category',width:'20%'},
                {name:'projectDetail.designStage',index:'designStage',width:'20%'},
                {name:'projectDetail.speciality',index:'speciality',width:'20%'},
                {name:'projectDetail.role',index:'role',width:'20%'},
                {name:'projectDetail.volume',index:'volume',width:'20%'},
                {name:'statusStr',index:'statusStr',width:'20%'},
                {name:'month',index:'month',width:'20%'},
                {name:'hours',index:'hours',width:'20%'}
            ],
            sortable:true,
            viewrecords : true,
//            rownumbers:true,//添加左侧行号
            rowNum:15,
            rowList:[15,20,30],
            pager : pager_selector,
            altRows: true,
            //toppager: true,

//            multiselect: true,
            //multikey: "ctrlKey",
//            multiboxonly: true,

            loadComplete : function() {
                var table = this;
                setTimeout(function(){
                    styleCheckbox(table);
                    updateActionIcons(table);
                    updatePagerIcons(table);
                    enableTooltips(table);
                }, 0);
            },

            editurl: "${ctx}/template/edit",//nothing is saved
//            caption: "hello",
            jsonReader: {
                root: "list",
                records:"total",
                total:"pages"
            },
            prmNames:{page:"page.pageNum",rows:"page.pageSize"}
        });


        $(window).triggerHandler('resize.jqGrid');//trigger window resize to make the grid get the correct size



        //enable search/filter toolbar
//        jQuery(grid_selector_s2).jqGrid('filterToolbar',{defaultSearch:true,stringResult:true})
//        jQuery(grid_selector_s2).filterToolbar({});


        //switch element when editing inline
        function aceSwitch( cellvalue, options, cell ) {
            setTimeout(function(){
                $(cell) .find('input[type=checkbox]')
                        .addClass('ace ace-switch ace-switch-5')
                        .after('<span class="lbl"></span>');
            }, 0);
        }
        //enable datepicker
        function pickDate( cellvalue, options, cell ) {
            setTimeout(function(){
                $(cell) .find('input[type=text]')
                        .datepicker({format:'yyyy-mm-dd' , autoclose:true});
            }, 0);
        }

        function style_edit_form(form) {
            //enable datepicker on "sdate" field and switches for "stock" field
            form.find('input[name=sdate]').datepicker({format:'yyyy-mm-dd' , autoclose:true})

            form.find('input[name=stock]').addClass('ace ace-switch ace-switch-5').after('<span class="lbl"></span>');
            //don't wrap inside a label element, the checkbox value won't be submitted (POST'ed)
            //.addClass('ace ace-switch ace-switch-5').wrap('<label class="inline" />').after('<span class="lbl"></span>');


            //update buttons classes
            var buttons = form.next().find('.EditButton .fm-button');
            buttons.addClass('btn btn-sm').find('[class*="-icon"]').hide();//ui-icon, s-icon
            buttons.eq(0).addClass('btn-primary').prepend('<i class="ace-icon fa fa-check"></i>');
            buttons.eq(1).prepend('<i class="ace-icon fa fa-times"></i>')

            buttons = form.next().find('.navButton a');
            buttons.find('.ui-icon').hide();
            buttons.eq(0).append('<i class="ace-icon fa fa-chevron-left"></i>');
            buttons.eq(1).append('<i class="ace-icon fa fa-chevron-right"></i>');
        }

        function style_delete_form(form) {
            var buttons = form.next().find('.EditButton .fm-button');
            buttons.addClass('btn btn-sm btn-white btn-round').find('[class*="-icon"]').hide();//ui-icon, s-icon
            buttons.eq(0).addClass('btn-danger').prepend('<i class="ace-icon fa fa-trash-o"></i>');
            buttons.eq(1).addClass('btn-default').prepend('<i class="ace-icon fa fa-times"></i>')
        }

        function style_search_filters(form) {
            form.find('.delete-rule').val('X');
            form.find('.add-rule').addClass('btn btn-xs btn-primary');
            form.find('.add-group').addClass('btn btn-xs btn-success');
            form.find('.delete-group').addClass('btn btn-xs btn-danger');
        }
        function style_search_form(form) {
            var dialog = form.closest('.ui-jqdialog');
            var buttons = dialog.find('.EditTable')
            buttons.find('.EditButton a[id*="_reset"]').addClass('btn btn-sm btn-info').find('.ui-icon').attr('class', 'ace-icon fa fa-retweet');
            buttons.find('.EditButton a[id*="_query"]').addClass('btn btn-sm btn-inverse').find('.ui-icon').attr('class', 'ace-icon fa fa-comment-o');
            buttons.find('.EditButton a[id*="_search"]').addClass('btn btn-sm btn-purple').find('.ui-icon').attr('class', 'ace-icon fa fa-search');
        }

        function beforeDeleteCallback(e) {
            var form = $(e[0]);
            if(form.data('styled')) return false;

            form.closest('.ui-jqdialog').find('.ui-jqdialog-titlebar').wrapInner('<div class="widget-header" />')
            style_delete_form(form);

            form.data('styled', true);
        }

        function beforeEditCallback(e) {
            var form = $(e[0]);
            form.closest('.ui-jqdialog').find('.ui-jqdialog-titlebar').wrapInner('<div class="widget-header" />')
            style_edit_form(form);
        }



        //it causes some flicker when reloading or navigating grid
        //it may be possible to have some custom formatter to do this as the grid is being created to prevent this
        //or go back to default browser checkbox styles for the grid
        function styleCheckbox(table) {
            /**
             $(table).find('input:checkbox').addClass('ace')
             .wrap('<label />')
             .after('<span class="lbl align-top" />')


             $('.ui-jqgrid-labels th[id*="_cb"]:first-child')
             .find('input.cbox[type=checkbox]').addClass('ace')
             .wrap('<label />').after('<span class="lbl align-top" />');
             */
        }


        //unlike navButtons icons, action icons in rows seem to be hard-coded
        //you can change them like this in here if you want
        function updateActionIcons(table) {
            /**
             var replacement =
             {
                 'ui-ace-icon fa fa-pencil' : 'ace-icon fa fa-pencil blue',
                 'ui-ace-icon fa fa-trash-o' : 'ace-icon fa fa-trash-o red',
                 'ui-icon-disk' : 'ace-icon fa fa-check green',
                 'ui-icon-cancel' : 'ace-icon fa fa-times red'
             };
             $(table).find('.ui-pg-div span.ui-icon').each(function(){
						var icon = $(this);
						var $class = $.trim(icon.attr('class').replace('ui-icon', ''));
						if($class in replacement) icon.attr('class', 'ui-icon '+replacement[$class]);
					})
             */
        }

        //replace icons with FontAwesome icons like above
        function updatePagerIcons(table) {
            var replacement =
            {
                'ui-icon-seek-first' : 'ace-icon fa fa-angle-double-left bigger-140',
                'ui-icon-seek-prev' : 'ace-icon fa fa-angle-left bigger-140',
                'ui-icon-seek-next' : 'ace-icon fa fa-angle-right bigger-140',
                'ui-icon-seek-end' : 'ace-icon fa fa-angle-double-right bigger-140'
            };
            $('.ui-pg-table:not(.navtable) > tbody > tr > .ui-pg-button > .ui-icon').each(function(){
                var icon = $(this);
                var $class = $.trim(icon.attr('class').replace('ui-icon', ''));

                if($class in replacement) icon.attr('class', 'ui-icon '+replacement[$class]);
            })
        }

        function enableTooltips(table) {
            $('.navtable .ui-pg-button').tooltip({container:'body'});
            $(table).find('.ui-pg-div').tooltip({container:'body'});
        }

        //var selr = jQuery(grid_selector).jqGrid('getGridParam','selrow');

        $(document).one('ajaxloadstart.page', function(e) {
            $.jgrid.gridDestroy(grid_selector);
            $('.ui-jqdialog').remove();
        });

        $("#form-search").submit(function() {
            $("#grid-table").jqGrid('setGridParam',{
                url:"${ctx}/hours/history?status=1",
                postData:{'page.searchStr':$("#nav-search-input").val()}, //发送数据
                page:1
            }).trigger("reloadGrid");
            return false;
        });

        $(".nav-search-input").datepicker({
            language:  'zh-CN',
            autoclose: true,
            format: "yyyy-mm",
            minViewMode: 1,
            todayBtn: false
        });



        $(".nav-search-input").datepicker("setDate", getCuMonth());

        function getCuMonth(){
            var date = new Date;
            var year=date.getFullYear();
            var month=date.getMonth()+1;
            month =(month<10 ? "0"+month:month);
            var mydate = (year.toString() + "-" + month.toString());
            return mydate;
        }
    });
</script>
</body>
</html>
